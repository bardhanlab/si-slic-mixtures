function [DG]= BORN(eps_hat,r,q,conv_factor)

    dGdN=-1./r.^2;
    Sigma= eps_hat * dGdN * q;
    DG = conv_factor * 0.5 * q * r .* Sigma;