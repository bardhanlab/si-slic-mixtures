function [SLIC_DG_Trans, Born_DG_Trans] = PlotCationsAnions2(path,funEps_s_list,funParams_list,exp_list,ConstituentSolvents,ion_ref)
global eps_in conv_factor export
tol = 1e-15;
cations = {'Li','Na','K','Rb','Cs'};
anions = {'Cl','Br','I'};

for i = 1:length(funEps_s_list)
    countercat = 1;
    counteran = 1;
    Eps_fun = funEps_s_list{i};
    Params = funParams_list{i};
    Exp = exp_list{i};
    for j = 1:length(ion_ref)
      q = ion_ref(j).q;
      r = ion_ref(j).r;
      index(i,j) = {find(strcmp(ion_ref(j).Name,{Exp.Ion}))};
      if not(isempty(index{i,j}))
          Concs{i,j} = [Exp(index{i,j}).Weight];
          if j <= length(cations)
          legendinfocations{i,countercat} = cations{j};
          countercat = countercat + 1;
          legendinfocations{i,countercat} = [cations{j}, ' EXP'];
          countercat = countercat + 1;
          end

      if j > length(cations) && j <= length(ion_ref)
          legendinfoanions{i,counteran} = anions{j-length(cations)};
          counteran = counteran + 1;
          legendinfoanions{i,counteran} = [anions{j-length(cations)}, ' EXP'];
          counteran = counteran + 1;
      end
      k = 1;
      Concentrations_ion = Concs{i,j};
      Concentrations(:,j,i) = linspace(min(Concentrations_ion),max(Concentrations_ion),100);
      while k <= length(Concentrations(:,j,i))   
          Eps = Eps_fun(Concentrations(k,j,i));
          Eps_neat = Eps_fun(0); 
          eps_hat = (Eps-eps_in)/(Eps);
          eps_hat_neat = (Eps_neat-eps_in)/Eps_neat;
          eps_hat_p = 0;
          dEps_dT=0;
          deltaT = 1e-4;
          d2Eps_dT2 = 0;
          eps_hat_p_p = (Eps^2*d2Eps_dT2 - 2*dEps_dT^2*Eps +2*(Eps-eps_in)*dEps_dT^2 ...
            - (Eps-eps_in)*d2Eps_dT2*Eps)/Eps^3;

          SLIC_DG_Trans(k,j,i)= NLBC_TANH(Concentrations(k,j,i),eps_hat,eps_hat_p,r,tol,q, ...
            Params,25,eps_in,[20 30] ,conv_factor,eps_hat_p_p,1,Concentrations(k,j,i)) - ...
            NLBC_TANH(0,eps_hat_neat,eps_hat_p,r,tol,q, ...
            Params,25,eps_in,[20 30] ,conv_factor,eps_hat_p_p,1,0);

          Born_DG_Trans(k,j,i) = BORN(eps_hat,r,q,conv_factor)-BORN(eps_hat_neat,r,q,conv_factor);
          k = k + 1;
      end
      end
    end
end

map=[0,0,0; 1,0,0; 0.5, 0, 0; 0.5, 0.5, 0; 0, 0.5, 0; 0, 0.5, 1; 0, 0, .5; 0.5, 0, 0.5; 0.5, 0.5 0.5];
symbolmap = {'h','d','p','^','o','s','v','+','*'};

for i = 1:length(ConstituentSolvents)
    cationlegend = legendinfocations(i,:);
    anionlegend = legendinfoanions(i,:);
    for j = 1:length(cations)
        Exp = exp_list{i};
        if not(all(SLIC_DG_Trans(:,j,i)==0))
            plot(Concentrations(:,j,i),SLIC_DG_Trans(:,j,i),'--','Color',map(j,:),'Linewidth',3)
            hold on
            plot([Exp(index{i,j}).Weight],[Exp(index{i,j}).DG_Trans_Exp],symbolmap{j},'Color',map(j,:),'Markersize',10,'MarkerFaceColor',map(j,:));
            xlabel(['Concentration of ', ConstituentSolvents{i}],'FontSize',20)
            ylabel('\Delta G^{\circ}_{tr}','FontSize',20)
            title(['\Delta G^{\circ}_{tr} for Cations in ',ConstituentSolvents{i}],'FontSize',20)
%             legend(cationlegend(~cellfun('isempty',cationlegend)),'Location','eastoutside')
%             set(legend,'FontSize',20);
            hold on
        end
        
    end
    if export == 1
       filename = sprintf([path{1},'/%s.pdf'],['DG_Cations_',ConstituentSolvents{i}]);
       export_fig(filename,'-painters','-transparent');
    end
    figure
    for j = length(cations)+1:length(cations)+length(anions)
        Exp = exp_list{i};
        if not(all(SLIC_DG_Trans(:,j,i)==0))
            plot(Concentrations(:,j,i),SLIC_DG_Trans(:,j,i),'--','Color',map(j,:),'Linewidth',3)
            hold on
            plot([Exp(index{i,j}).Weight],[Exp(index{i,j}).DG_Trans_Exp],symbolmap{j},'Color',map(j,:),'Markersize',10,'MarkerFaceColor',map(j,:));
            xlabel(['Concentration of ', ConstituentSolvents{i}],'FontSize',20)
            ylabel('\Delta G^{\circ}_{tr}','FontSize',20)
            title(['\Delta G^{\circ}_{tr} for Anions in ',ConstituentSolvents{i}],'FontSize',20)
%             legend(anionlegend(~cellfun('isempty',anionlegend)),'Location','eastoutside')
%             set(legend,'FontSize',20);
            hold on
        end 
    end
    if export == 1
       filename = sprintf([path{2},'/%s.pdf'],['DG_Anions_',ConstituentSolvents{i}]);
       export_fig(filename,'-painters','-transparent');
    end
    if i ~= length(ConstituentSolvents) 
        figure
    end
end