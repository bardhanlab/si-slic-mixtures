global order_toggle fill

if order_toggle == 2
    fileSolv = 'Output/RMS/rms_solv.tex';
    fileTrans = 'Output/RMS/rms_trans.tex';
    fileBoth = 'Output/RMS/rms_both.tex';
elseif order_toggle == 1 && fill == 0
    fileSolv = 'Output/RMS/rms_solv_linear.tex';
    fileTrans = 'Output/RMS/rms_trans_linear.tex';
    fileBoth = 'Output/RMS/rms_both_linear.tex';
elseif order_toggle == 1 && fill == 1
    fileSolv = 'Output/RMS/rms_solv_linear_fill.tex';
    fileTrans = 'Output/RMS/rms_trans_linear_fill.tex';
    fileBoth = 'Output/RMS/rms_both_linear_fill.tex';
end


colstructure = struct('Li',1,'Na',2,'K',3,'Rb',4,'Cs',5,'Cl',6,'Br',7,'I',8);
rowstructure = struct('MeOH',1,'DMSO',2,'AC',3,'EtOH',4,'AN',5,'Urea',6,'DME',7,'Diox',8,'DMF',9);

for i=1:length(MeOH_Ion_Struct)
MeOH_q(i)= MeOH_Ion_Struct(i).Charge;
MeOH_transfer(i) = MeOH_Ion_Struct(i).DG_Trans_Exp;
MeOH_solv(i) = MeOH_Ion_Struct(i).DG_Exp;
[myrow,mycol] = getIndex('MeOH',MeOH_Ion_Struct(i).Ion,rowstructure,colstructure);
NeatWaterSolvationForMeOHProblems(i) = DG_solv_table(myrow,mycol);
end
error_solv_MeOH=MeOH_solv'-(Calculated_Results_MeOH'+NeatWaterSolvationForMeOHProblems');
error_trans_MeOH = MeOH_transfer'-Calculated_Results_MeOH';
cation_solv_MeOH = error_solv_MeOH(find(MeOH_q==1));
anion_solv_MeOH = error_solv_MeOH(find(MeOH_q==-1));
cation_trans_MeOH = error_trans_MeOH(find(MeOH_q==1));
anion_trans_MeOH = error_trans_MeOH(find(MeOH_q==-1));


for i=1:length(EtOH_Ion_Struct)
EtOH_q(i)= EtOH_Ion_Struct(i).Charge;
EtOH_transfer(i) = EtOH_Ion_Struct(i).DG_Trans_Exp;
EtOH_solv(i) = EtOH_Ion_Struct(i).DG_Exp;
[myrow,mycol] = getIndex('EtOH',EtOH_Ion_Struct(i).Ion,rowstructure,colstructure);
NeatWaterSolvationForEtOHProblems(i) = DG_solv_table(myrow,mycol);
end
error_solv_EtOH=EtOH_solv'-(Calculated_Results_EtOH'+NeatWaterSolvationForEtOHProblems');
error_trans_EtOH = EtOH_transfer'-Calculated_Results_EtOH';
cation_solv_EtOH = error_solv_EtOH(find(EtOH_q==1));
anion_solv_EtOH = error_solv_EtOH(find(EtOH_q==-1));
cation_trans_EtOH = error_trans_EtOH(find(EtOH_q==1));
anion_trans_EtOH = error_trans_EtOH(find(EtOH_q==-1));

for i=1:length(AN_Ion_Struct)
AN_q(i)= AN_Ion_Struct(i).Charge;
AN_transfer(i) = AN_Ion_Struct(i).DG_Trans_Exp;
AN_solv(i) = AN_Ion_Struct(i).DG_Exp;
[myrow,mycol] = getIndex('AN',AN_Ion_Struct(i).Ion,rowstructure,colstructure);
NeatWaterSolvationForANProblems(i) = DG_solv_table(myrow,mycol);
end
error_solv_AN=AN_solv'-(Calculated_Results_AN'+NeatWaterSolvationForANProblems');
error_trans_AN = AN_transfer'-Calculated_Results_AN';
cation_solv_AN = error_solv_AN(find(AN_q==1));
anion_solv_AN = error_solv_AN(find(AN_q==-1));
cation_trans_AN = error_trans_AN(find(AN_q==1));
anion_trans_AN = error_trans_AN(find(AN_q==-1));


for i=1:length(Diox_Ion_Struct)
Diox_q(i)= Diox_Ion_Struct(i).Charge;
Diox_transfer(i) = Diox_Ion_Struct(i).DG_Trans_Exp;
Diox_solv(i) = Diox_Ion_Struct(i).DG_Exp;
[myrow,mycol] = getIndex('Diox',Diox_Ion_Struct(i).Ion,rowstructure,colstructure);
NeatWaterSolvationForDioxProblems(i) = DG_solv_table(myrow,mycol);
end
error_solv_Diox=Diox_solv'-(Calculated_Results_Diox'+NeatWaterSolvationForDioxProblems');
error_trans_Diox = Diox_transfer'-Calculated_Results_Diox';
cation_solv_Diox = error_solv_Diox(find(Diox_q==1));
anion_solv_Diox = error_solv_Diox(find(Diox_q==-1));
cation_trans_Diox = error_trans_Diox(find(Diox_q==1));
anion_trans_Diox = error_trans_Diox(find(Diox_q==-1));

for i=1:length(DME_Ion_Struct)
DME_q(i)= DME_Ion_Struct(i).Charge;
DME_transfer(i) = DME_Ion_Struct(i).DG_Trans_Exp;
DME_solv(i) = DME_Ion_Struct(i).DG_Exp;
[myrow,mycol] = getIndex('DME',DME_Ion_Struct(i).Ion,rowstructure,colstructure);
NeatWaterSolvationForDMEProblems(i) = DG_solv_table(myrow,mycol);
end
error_solv_DME=DME_solv'-(Calculated_Results_DME'+NeatWaterSolvationForDMEProblems');
error_trans_DME = DME_transfer'-Calculated_Results_DME';
cation_solv_DME = error_solv_DME(find(DME_q==1));
anion_solv_DME = error_solv_DME(find(DME_q==-1));
cation_trans_DME = error_trans_DME(find(DME_q==1));
anion_trans_DME = error_trans_DME(find(DME_q==-1));


for i=1:length(DMF_Ion_Struct)
DMF_q(i)= DMF_Ion_Struct(i).Charge;
DMF_transfer(i) = DMF_Ion_Struct(i).DG_Trans_Exp;
DMF_solv(i) = DMF_Ion_Struct(i).DG_Exp;
[myrow,mycol] = getIndex('DMF',DMF_Ion_Struct(i).Ion,rowstructure,colstructure);
NeatWaterSolvationForDMFProblems(i) = DG_solv_table(myrow,mycol);
end
error_solv_DMF=DMF_solv'-(Calculated_Results_DMF'+NeatWaterSolvationForDMFProblems');
error_trans_DMF = DMF_transfer'-Calculated_Results_DMF';
cation_solv_DMF = error_solv_DMF(find(DMF_q==1));
anion_solv_DMF = error_solv_DMF(find(DMF_q==-1));
cation_trans_DMF = error_trans_DMF(find(DMF_q==1));
anion_trans_DMF = error_trans_DMF(find(DMF_q==-1));

for i=1:length(DMSO_Ion_Struct)
DMSO_q(i)= DMSO_Ion_Struct(i).Charge;
DMSO_transfer(i) = DMSO_Ion_Struct(i).DG_Trans_Exp;
DMSO_solv(i) = DMSO_Ion_Struct(i).DG_Exp;
[myrow,mycol] = getIndex('DMSO',DMSO_Ion_Struct(i).Ion,rowstructure,colstructure);
NeatWaterSolvationForDMSOProblems(i) = DG_solv_table(myrow,mycol);
end
error_solv_DMSO=DMSO_solv'-(Calculated_Results_DMSO'+NeatWaterSolvationForDMSOProblems');
error_trans_DMSO = DMSO_transfer'-Calculated_Results_DMSO';
cation_solv_DMSO = error_solv_DMSO(find(DMSO_q==1));
anion_solv_DMSO = error_solv_DMSO(find(DMSO_q==-1));
cation_trans_DMSO = error_trans_DMSO(find(DMSO_q==1));
anion_trans_DMSO = error_trans_DMSO(find(DMSO_q==-1));


for i=1:length(Urea_Ion_Struct)
Urea_q(i)= Urea_Ion_Struct(i).Charge;
Urea_transfer(i) = Urea_Ion_Struct(i).DG_Trans_Exp;
Urea_solv(i) = Urea_Ion_Struct(i).DG_Exp;
[myrow,mycol] = getIndex('Urea',Urea_Ion_Struct(i).Ion,rowstructure,colstructure);
NeatWaterSolvationForUreaProblems(i) = DG_solv_table(myrow,mycol);
end
error_solv_Urea=Urea_solv'-(Calculated_Results_Urea'+NeatWaterSolvationForUreaProblems');
error_trans_Urea = Urea_transfer'-Calculated_Results_Urea';
cation_solv_Urea = error_solv_Urea(find(Urea_q==1));
anion_solv_Urea = error_solv_Urea(find(Urea_q==-1));
cation_trans_Urea = error_trans_Urea(find(Urea_q==1));
anion_trans_Urea = error_trans_Urea(find(Urea_q==-1));


for i=1:length(AC_total_info)
AC_q(i)= AC_total_info(i).Charge;
AC_transfer(i) = AC_total_info(i).DG_Trans_Exp;
AC_solv(i) = AC_total_info(i).DG_Exp;
[myrow,mycol] = getIndex('AC',AC_total_info(i).Ion,rowstructure,colstructure);
NeatWaterSolvationForACProblems(i) = DG_solv_table(myrow,mycol);
end
error_solv_AC=AC_solv'-(Calculated_Results_AC'+NeatWaterSolvationForACProblems');
error_trans_AC = AC_transfer'-Calculated_Results_AC';
cation_solv_AC = error_solv_AC(find(AC_q==1));
anion_solv_AC = error_solv_AC(find(AC_q==-1));
cation_trans_AC = error_trans_AC(find(AC_q==1));
anion_trans_AC = error_trans_AC(find(AC_q==-1));

RMSsolv = [rms(cation_solv_MeOH) rms(anion_solv_MeOH);
	   rms(cation_solv_DMSO) rms(anion_solv_DMSO);
	   rms(cation_solv_AC) rms(anion_solv_AC);
	   rms(cation_solv_EtOH) rms(anion_solv_EtOH);
	   rms(cation_solv_AN) rms(anion_solv_AN);
	   rms(cation_solv_Urea) rms(anion_solv_Urea);
	   rms(cation_solv_DME) rms(anion_solv_DME);
	   rms(cation_solv_Diox) rms(anion_solv_Diox);
	   rms(cation_solv_DMF) rms(anion_solv_DMF);];
	   
RMStrans = [rms(cation_trans_MeOH) rms(anion_trans_MeOH);
	   rms(cation_trans_DMSO) rms(anion_trans_DMSO);
	   rms(cation_trans_AC) rms(anion_trans_AC);
	   rms(cation_trans_EtOH) rms(anion_trans_EtOH);
	   rms(cation_trans_AN) rms(anion_trans_AN);
	   rms(cation_trans_Urea) rms(anion_trans_Urea);
	   rms(cation_trans_DME) rms(anion_trans_DME);
	   rms(cation_trans_Diox) rms(anion_trans_Diox);
	   rms(cation_trans_DMF) rms(anion_trans_DMF);];
	   
matrix2latex(round(RMSsolv,2),fileSolv,...
	     'rowLabels',{'MeOH','DMSO','AC','EtOH','AN','Urea', ...
		    'DME','Diox','DMF'},'columnlabels',{'Cations', 'Anions'},'Predict','RMS Solvation');

matrix2latex(round(RMStrans,2),fileTrans,...
	     'rowLabels',{'MeOH','DMSO','AC','EtOH','AN','Urea', ...
		    'DME','Diox','DMF'},'columnlabels',{'Cations', 'Anions'},'Predict','RMS Transfer');

matrix2latex(round([RMSsolv RMStrans],2),fileBoth,...
	     'rowLabels',{'MeOH','DMSO','AC','EtOH','AN','Urea', ...
		    'DME','Diox','DMF'},'columnlabels',{'Cations $\Delta G_{solv}^{es}$', 'Anions $\Delta G_{solv}^{es}$','Cations $\Delta G_{tr}^{\circ}$', 'Anions $\Delta G_{tr}^{\circ}$'},'Predict','RMS Both');
