function SLIC_Error = calcError(Exp_Water, SLIC_Water, ion_info, Eps_Fun, Params)
global eps_in conv_factor
varname = inputname(3);
Solvent = strsplit(varname,'_');
Solvent = Solvent{1};
[m2, index2] = ismember(Solvent,{SLIC_Water.Solvents});
temperature = 25; 
tol = 1e-15;
predict = 1;
Ions = {'Li', 'Na', 'K', 'Rb', 'Cs', 'Cl', 'Br', 'I'};
Ion_order = struct('Ions', Ions, 'Order', {1,2,3,4,5,6,7,8});
SLIC = rmfield(SLIC_Water,'Solvents');
SLIC = SLIC(index2,:);
temp = struct2cell(SLIC);
SLIC_Wat = cell2mat(temp);


Neat_Error = Exp_Water - transpose(SLIC_Wat); 

max_Conc = 0;
for i = 1:length(Ions)
    [~,index] = find(strcmp(Ions{i},{ion_info.Ion}));
    ConcentrationsLen = length([ion_info(index).Weight]);
    if ConcentrationsLen > max_Conc
       max_Conc = ConcentrationsLen;
    end
end


for i = 1:length(Ions)
    [m,index] = find(strcmp(Ions{i},{ion_info.Ion}));
    Concentrations = [ion_info(index).Weight];
    q = mean([ion_info(index).Charge]);
    r = mean([ion_info(index).Radius]);
    Exp_DG = [ion_info(index).DG_Exp];
    Exp_DG_Trans = [ion_info(index).DG_Trans_Exp];
    if m
       Name = Ions{i}; 
       [~,Col_index] = ismember(Name,{Ion_order.Ions});

    DG_NLBC_TANH = zeros(max_Conc,1);
    for j = 1:length(Concentrations)
        Eps=Eps_Fun(Concentrations(j));
        eps_hat = (Eps-eps_in)/(Eps);
        eps_hat_p = 0;
        dEps_dT=0;
        deltaT = 1e-4;
        d2Eps_dT2 = 0;
        eps_hat_p_p = (Eps^2*d2Eps_dT2 - 2*dEps_dT^2*Eps +2*(Eps-eps_in)*dEps_dT^2 ...
        - (Eps-eps_in)*d2Eps_dT2*Eps)/Eps^3;
       [DG_NLBC_TANH(j)]= ...                               %Function for symmetric NLBC TANH
        NLBC_TANH(Concentrations(j),eps_hat,eps_hat_p,r,tol,q, ...
        Params,temperature,eps_in,[20 30] ,conv_factor,eps_hat_p_p,predict,Concentrations(j));
    end
    
    DG_Trans = DG_NLBC_TANH(DG_NLBC_TANH~=0) - SLIC_Wat(Col_index);
    DG_Error = [Exp_DG-transpose(DG_NLBC_TANH(DG_NLBC_TANH~=0))];
    DG_Trans_Error = [Exp_DG_Trans-transpose(DG_Trans)];
    SLIC_Error(i) = struct('Name', Name,'Concentrations',Concentrations,'Neat_Water_Error',Neat_Error(i),'Error_DG',DG_Error, 'Error_DG_Trans', DG_Trans_Error, 'Total_Error',DG_Error-DG_Trans_Error);
    end
end
