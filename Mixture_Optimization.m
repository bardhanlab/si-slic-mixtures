function [Comp, Actual, Result, finalParams, Ion_Struct, Comp_BORN, DG_NLBC_TANH, Wat_Ref, Final_Diff] = Mixture_Optimization(problem,ConstituentSolvents,Ref_BORN)
predict = 0;
% addpath('../pnas2016/Phi_toggled')
% addpath('../pnas2016/Phi_toggled/export_fig/')
init = 0;
% set the following to 0 (zero) if you don't want to save each
% individual figure automatically, according to the names set
% inside the code.
 

global solvent_type solvent phi_toggle eps_in conv_factor FID errorgraph FID_PREDICT  FID2 printOn hfuncgraph
FID=fopen('param_data.txt','w');
FID_PREDICT=fopen('predict.txt','w');
FID2=fopen('parinfo.txt','w');
errorgraph=0;
heatcapgraph=0;
deltaggraph=0;
deltasgraph=0;
hfuncgraph=0;
printOn = 0;


eps_in=1;
conv_factor = 332.112 * 4.184;      % from Angstroms/relative
                                        % permittivities/electron charges                                        % into kJ/mol;
Actual = [problem.DG_Exp];

% Wat_Ref = RefDG;


% clearvars   dphidn_HSBC_MSA sigma_over_dphidn_HSBC_MSA ...
%             dphidn_NLBC_TANH sigma_nom_over_dphidn_NLBC_TANH...
%             dphidn_BORN sigma_over_dphidn_BORN ...
%             DG_HSBC_MSA DS_HSBC_MSA...
%             DG_Fawcett_Asym DS_Fawcett_Asym dphidn_Fawcett_Asym sigma_over_dphidn_Fawcett_Asym...
%             DG_Fawcett_Sym DS_Fawcett_Sym dphidn_Fawcett_Sym sigma_over_dphidn_Fawcett_Sym...
%             DG_NLBC_TANH DS_NLBC_TANH...        
%             DG_BORN DS_BORN CP_BORN...
%             CP_NLBC_TANH 


phi_toggle=1;

[DG_NLBC_TANH, Params_over_var(:,:),Ion_Struct,DG_BORN,Final_Diff] = Calcsolventmodels_MSA(1,init,problem,predict,ConstituentSolvents);
tol=1e-15;   
t = 25;
for i = 1:length(problem)
    TSV = [20 30];
    TEV = [20 30];
    TInterval=[20 30];
    eps_hat = ((problem(i).Eps_s_Neat_water)-eps_in)/(problem(i).Eps_s_Neat_water);
    eps_hat_p=((0)*(problem(i).Eps_s)...
        -(0)*((problem(i).Eps_s)-eps_in))/((problem(i).Eps_s))^2;

    Eps=problem(i).Eps_s;
    dEps_dT= 0; % total_solvent_info(index).deps_dt(temp);
    deltaT = 1e-4;
    d2Eps_dT2 = (0- ...
        0)/(2*deltaT); % for now. 
    eps_hat_p_p = (Eps^2*d2Eps_dT2 - 2*dEps_dT^2*Eps +2*(Eps-eps_in)*dEps_dT^2 ...
        - (Eps-eps_in)*d2Eps_dT2*Eps)/Eps^3;

    q=problem(i).Charge;
    r=problem(i).Radius;
    
    Wat_Ref(i) = NLBC_TANH(0,eps_hat,eps_hat_p,r,tol,q,Params_over_var,t,eps_in,TInterval,conv_factor,eps_hat_p_p,predict,0);
end
Wat_Ref = Wat_Ref;
Comp = -1.*(Wat_Ref - DG_NLBC_TANH(:,:,1));
Comp_BORN = -1.*(Ref_BORN - DG_BORN(:,:,1));

finalParams = Params_over_var;
try
Result = Actual - Comp;
catch
Result = 0;
display('Not enough reference data to compute difference')
end