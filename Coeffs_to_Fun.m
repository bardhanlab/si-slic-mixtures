function Funs = Coeffs_to_Fun(Params)
global order_toggle

alpha = Params(:,1);
beta = Params(:,2);
gamma = Params(:,3);
mu = Params(:,4);
phi_stat = Params(:,5);

if order_toggle == 2
Alpha_Fun = @(X) alpha(1).*X.^2+alpha(2).*X+alpha(3);
Beta_Fun = @(X) beta(1).*X.^2+beta(2).*X+beta(3);
Gamma_Fun = @(X) gamma(1).*X.^2+gamma(2).*X+gamma(3);
Mu_Fun = @(X) mu(1).*X.^2+mu(2).*X+mu(3);
Phi_Stat_Fun = @(X) phi_stat(1).*X.^2+phi_stat(2).*X+phi_stat(3);
elseif order_toggle == 1
Alpha_Fun = @(X) alpha(1).*X+alpha(2);
Beta_Fun = @(X) beta(1).*X+beta(2);
Gamma_Fun = @(X) gamma(1).*X+gamma(2);
Mu_Fun = @(X) mu(1).*X+mu(2);
Phi_Stat_Fun = @(X) phi_stat(1).*X+phi_stat(2);
  
end
% Alpha_Fun = @(X) alpha(1).*X.^2+alpha(2).*X+0.0988559374840066;
% Beta_Fun = @(X) beta(1).*X.^2+beta(2).*X+41.2467242078621;
% Gamma_Fun = @(X) gamma(1).*X.^2+gamma(2).*X+19.8172137324093;
% Mu_Fun = @(X) mu(1).*X.^2+mu(2).*X+0.405895271807237;
% Phi_Stat_Fun = @(X) phi_stat(1).*X.^2+phi_stat(2).*X-0.00792594052764437;

% Alpha_Fun = @(X) alpha(1).*X+0.0988559374840066;
% Beta_Fun = @(X) beta(1).*X+41.2467242078621;
% Gamma_Fun = @(X) gamma(1).*X+19.8172137324093;
% Mu_Fun = @(X) mu(1).*X+0.405895271807237;
% Phi_Stat_Fun = @(X) phi_stat(1).*X-0.00792594052764437;

% Alpha_Fun = @(X) alpha(1).*X+0.09940595567;
% Beta_Fun = @(X) beta(1).*X+41.248099259;
% Gamma_Fun = @(X) gamma(1).*X+19.81692877502;
% Mu_Fun = @(X) mu(1).*X+0.40579551105;
% Phi_Stat_Fun = @(X) phi_stat(1).*X-0.00802203755;

Funs = struct('Params',{'alpha', 'beta', 'gamma', 'mu', 'phi_stat'},'Functions', {Alpha_Fun Beta_Fun Gamma_Fun Mu_Fun Phi_Stat_Fun});
