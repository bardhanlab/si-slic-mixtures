function Radius = Param_radius(eps_hat,Energy,q,conv_factor)

Radius = (- conv_factor * q.^2 .* eps_hat)./(2*Energy);