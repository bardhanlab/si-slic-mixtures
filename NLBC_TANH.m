function [DG,En,dphidn,Sigma]= NLBC_TANH(weight,eps_hat,eps_hat_p,r,tol,q,solventinfo,t,eps_in,TInterval,conv_factor,eps_hat_p_p,predict,c)
if isstruct(solventinfo)
    alpha =     solventinfo.alpha_tanh_fit(c);
    beta  =     solventinfo.beta_tanh_fit(c);
    gamma =     solventinfo.gamma_tanh_fit(c);
    mu =        solventinfo.mu_tanh_fit(c);
    phi_stat=   solventinfo.phi_stat_tanh_fit(c);


    dGdN = -1./r.^2;
    dphidn=-q./r.^2;
    ERROR=1e10;
    sigma_0=eps_hat*q*dGdN;
    while ERROR>tol
        sigma=sigma_0;
        E_n_p=-q*dGdN-(-0.5)*sigma; 
        hf=alpha*tanh(beta*(E_n_p)-gamma)+mu;
        sigma_0=eps_hat*q*dGdN./(1+hf);
        ERROR1=max(abs((sigma-sigma_0)./sigma_0));
        ERROR2=max(abs(sigma-sigma_0));
        ERROR=max(ERROR1,ERROR2);
    end
    Sigma=sigma_0;   
    En=-q*dGdN-(-0.5)*Sigma;

    DG=0.5*conv_factor*r.*Sigma*q+phi_stat*q*conv_factor;
else
    [alpha, beta, gamma, mu, phi_stat] = Calc_Params(weight,solventinfo(:,1), ...
        solventinfo(:,2),solventinfo(:,3),solventinfo(:,4),solventinfo(:,5));


    dGdN = -1./r.^2;
    dphidn=-q./r.^2;
    ERROR=1e10;
    sigma_0=eps_hat*q*dGdN;
    while ERROR>tol
        sigma=sigma_0;
        E_n_p=-q*dGdN-(-0.5)*sigma; 
        hf=alpha*tanh(beta*(E_n_p)-gamma)+mu;
        sigma_0=eps_hat*q*dGdN./(1+hf);
        ERROR1=max(abs((sigma-sigma_0)./sigma_0));
        ERROR2=max(abs(sigma-sigma_0));
        ERROR=max(ERROR1,ERROR2);
    end
    Sigma=sigma_0;   
    En=-q*dGdN-(-0.5)*Sigma;

    DG=0.5*conv_factor*r.*Sigma*q+phi_stat*q*conv_factor;
end
    