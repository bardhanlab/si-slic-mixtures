function [Coeffs, Fun] = SolventEps(Independant,Eps,degree)

Coeffs = polyfit(Independant,Eps,degree);
Fun = @(X) Coeffs(1).*(X).^2+Coeffs(2).*(X)+Coeffs(3);
