function [x,Ion_strcut,Final_Diff]=mytanhopt_temp(t,index,x0,init,ions,ConstituentSolvents)
global phi_toggle conv_factor order_toggle 
global solvent 
options = optimoptions('lsqnonlin');
options = optimoptions(options,'Display', 'off');
if order_toggle == 2
    options = optimoptions(options, 'TolFun',5e-9);
elseif order_toggle == 1
    options = optimoptions(options, 'TolFun',1e-6);
end   
% options = optimoptions(options, 'TypicalX', [-0.000000594656863, 0.000001517638887, -0.000003114239476, -0.000002001423576, -0.000000757011536, ...
%     -0.000094496371346, -0.000282668534354, 0.000579219632818, 0.000132971648018, -0.000017219270393, ...
%     0.099405955673658, 41.248099258594202, 19.816928775019399, 0.405795511045443, -0.008022037545124]);

if phi_toggle == 0 && init == 1
    if strcmp(solvent(index).Name,'PrOH')
        lb = [-10 -100 -10 -10];
        ub = [10 100 10 10];
    elseif init == 1
        lb = [-inf -inf -inf -inf];
        ub = [inf inf inf inf];
    end
    
elseif phi_toggle == 1 && init == 1
    if strcmp(solvent(index).Name,'PrOH')
        lb = [-10 -100 -10 -10 1];
        ub = [10 100 10 10 -1];
    elseif init == 1
        lb = [-inf -inf -inf -inf  -inf];
        ub = [inf inf inf inf inf];
    end
    
else 
    if order_toggle == 2
        lb = [-inf -inf -inf -inf  -inf;-inf -inf -inf -inf  -inf;-inf -inf -inf -inf  -inf];
    elseif order_toggle == 1
        lb = [-inf -inf -inf -inf  -inf;-inf -inf -inf -inf  -inf];        
    end
        %     lb = -1e-3.*[1 1 1 1 1; 1 1 1 1 1; 10000 100000 100000 10000 10000];
%     lb = [-inf -inf -inf -inf  -inf;-inf -inf -inf -inf  -inf];
%     lb = [-inf -inf -inf -inf  -inf];
    ub = -1.*lb;
%     ub = 1e-3.*[1 1 1 1 1 ; 1 1 1 1 1 ; 10000 100000 100000 10000 10000];
end
    for i = 1:length(t) 
        if init == 1
            [Ref,Ion_strcut] = Calc_DeltaG_exp(t(i),solvent(index),index,init,ions);
        end
            error=1e10;
        if init == 1
            y=@(x) amsa_temp(x,t(i),Ref,index);
            y2 = @(x) bornPicardNoStern(x,Ref,ions,init,1,332.112,10);
        else
            Ion_strcut = ions;
            Ref = [ions.DG_Exp]./conv_factor;
            y=@(x) amsa_temp_glob(x,t(i),Ref,index,ions, init);
            y2 = @(x) bornPicardNoStern(x,Ref,ions,init,1,332.112*4.184,10);
        end
            while error>1e-10            
%                x(i,:)=fminsearch(y,x0,options);
                 if init == 1 || init == 0
                    [x(:,:),~,Final_Diff]=lsqnonlin(y,x0,lb,ub,options);
                 else
                     options = optimoptions('fmincon','Display','iter','Algorithm','sqp');
                     A = [100^2 100 1 0 0 0 0 0 0 0 0 0 0 0 0; ...
                          -100^2 -100 -1 0 0 0 0 0 0 0 0 0 0 0 0; ...
                          0 0 0 100^2 100 1 0 0 0 0 0 0 0 0 0; ...
                          0 0 0 -100^2 -100 -1 0 0 0 0 0 0 0 0 0; ...
                          0 0 0 0 0 0 100^2 100 1 0 0 0 0 0 0; ...
                          0 0 0 0 0 0 -100^2 -100 -1 0 0 0 0 0 0; ...
                          0 0 0 0 0 0 0 0 0 100^2 100 1 0 0 0; ...
                          0 0 0 0 0 0 0 0 0 0 0 0 100^2 100 1];
                     b = [2;
                         2;
                         60;
                         0;
                         25;
                         0;
                          3;
                         1];
                     c = [];
                     Aeq = [0 0 1 0 0 0 0 0 0 0 0 0 0 0 0; ...
                            0 0 0 0 0 1 0 0 0 0 0 0 0 0 0; ...
                            0 0 0 0 0 0 0 0 1 0 0 0 0 0 0; ...
                            0 0 0 0 0 0 0 0 0 0 0 1 0 0 0; ...
                            0 0 0 0 0 0 0 0 0 0 0 0 0 0 1];
                     beq = [0.099405955673658; 41.248099258594202; 19.816928775019399; 0.405795511045443; -0.008022037545124];   
                     [x]=fmincon(y,x0,A,b,Aeq,beq,lb,ub,c,options);
                 end
                 error=norm(x(:,:)-x0);
                 x0=x(:,:);
            end
        if init == 1
            fprintf('%5s \n',['Optimized at ', num2str(t(i)) ,' C for solvent: ', num2str(solvent(index).Name)]);
        else
            fprintf('%5s \n',['Optimized at ', num2str(t(i)) ,' C for solvent: ',ConstituentSolvents{1},'-',ConstituentSolvents{2},' mixtures']);
        end            
%     fileID = fopen('Parameters_At_25_C.txt', 'a');
%     fprintf(fileID,'%6s %18s %12s %12s %12s %12s \r\n','Solvent','Temperature (C)','Alpha','Beta','Gamma','Mu'); 
%     fprintf(fileID,'-------------------------------------------------------------------------------------------\r\n');
%     fprintf(fileID,'%6s %12.2f %20.8f %12.8f %12.8f %12.8f \r\n',num2str(solvent(j).Name),t(i),x(1,1,j),x(1,2,j),x(1,3,j),x(1,4,j));
%     fclose(fileID);
    end
foo = 0;