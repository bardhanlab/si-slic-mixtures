function [Charge, Radius, Name] = Get_ion_info(Ion_Ref,Solvent_Ion_Struct)

for i = 1:length(Solvent_Ion_Struct)
   [m,index] = ismember( Solvent_Ion_Struct(i).Ion,{Ion_Ref.Name});
   if m
      Charge =  Ion_Ref(index).q;
      Radius = Ion_Ref(index).r;
      Name = Solvent_Ion_Struct(i).Ion;
   end
end