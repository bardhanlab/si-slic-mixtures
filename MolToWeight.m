function [Weight_Weight] = MolToWeight(Mol_fracs,Solvent1ConvFactor,Solvent2ConvFactor)

Mol_frac_Solvent1 = Mol_fracs;
Mol_frac_Solvent2 = 1-Mol_frac_Solvent1;

Solvent1_mass = 100*Mol_frac_Solvent1*Solvent1ConvFactor;
Solvent2_mass = 100*Mol_frac_Solvent2*Solvent2ConvFactor;

Total_mass = Solvent1_mass + Solvent2_mass;

Weight_Weight = Solvent1_mass./Total_mass;