function [DG_NLBC_TANH,Params_over_time, Ion_Struct,DG_BORN,Final_Diff] =Calcsolventmodels_MSA(index,init,ions,predict,ConstituentSolvents)


    tol=1e-15; 

    
    TSV = [];
    TEV = [];
    
    Param_index = 1;

global order_toggle eps_hat eps_in conv_factor  Ion_Data temp Parameters
temp=25;
if init == 1
    x_init = [0.11151568 40.96797198 20.40752039 0.47724794 0];
elseif strcmp(ConstituentSolvents{2}, 'MeOH');
    x_init = [-0.000000594656863, 0.000001517638887, -0.000003114239476, -0.000002001423576, -0.000000757011536;
                -0.000094496371346, -0.000282668534354, 0.000579219632818, 0.000132971648018, -0.000017219270393;
                0.099405955673658, 41.248099258594202, 19.816928775019399, 0.405795511045443, -0.008022037545124];
    
    x_init = [0.00796254747403963,1.12755086666498,-0.0437473905277195,-0.000741878466714399,-0.0127562550156597;
        -0.0352005345456820,0.385148242822066,0.328570371573210,0.0105861533462843,0.00327984149763509;
        0.158303901490002,3.25284605972790,0.969961416534694,0.393418036015349,-0.0176278922958045];

elseif strcmp(ConstituentSolvents{2}, 'AC');
    x_init = [-0.000009844454273, 0.000085867828865, -0.000181034920918, -0.000015769976080, 0.000000883985318;
                0.001415283973000, -0.010680636865994, 0.022517959004888, 0.001770757116243, -0.000188607913381;
                0.0988559374840066, 41.2467242078621, 19.8172137324093, 0.405895271807237, -0.00792594052764437];

x_init = [-0.0181206962135658,8.58678288650000e-05,-0.000181034920918000,0.0180950806179902,0.00934944873629272;
    -0.00294456996851099,-0.0106806350372067,0.0225179571761007,0.00613072492995726,-0.0191505649029729;
    0.0992011431268254,41.2467242383322,19.8172136307997,0.405550842892393,-0.00792133289167496];

elseif strcmp(ConstituentSolvents{2}, 'DMSO');
    x_init = [-0.000001674600647, 0.000003229065018, -0.000006622068631, -0.000000925131054, 0.000001426016888;
                -0.000060045592668, 0.000250776216035, -0.000514891868475, -0.000066906356894, -0.000147551540758;
                0.099405955673658, 41.248099258594202, 19.816928775019399, 0.405795511045443, -0.008022037545124];
elseif strcmp(ConstituentSolvents{2}, 'EtOH');
    x_init = [-0.000013025316520, 0.000088788360886, -0.000187192270993, -0.000013460310676, 0.000002676470730;
                0.001569557758367, -0.010920968388163, 0.023024649329185, 0.001688174399892, -0.000204973559912;
                0.099405955673658, 41.248099258594202, 19.816928775019399, 0.405795511045443, -0.008022037545124];
elseif strcmp(ConstituentSolvents{2}, 'AN');
    x_init = [-0.000001984848539, -0.000007657785552, 0.000015522137940, 0.000004327671841, 0.000002240443117;
                -0.000032362901378, 0.000254153625012, -0.000514090687708, -0.000200661393424, -0.000230877617108;
                0.099405955673658, 41.248099258594202, 19.816928775019399, 0.405795511045443, -0.008022037545124];
    x_init = [0, 0, 0, 0, 0; 
            0, 0, 0, 0, 0; 
            0.099405955673658, 41.248099258594202, 19.816928775019399, 0.004057955110454e2, -0.008022037545124];
    x_init = [0.187271345377272,8.54415001808701,1.41681911213757,-0.105508604991800,-0.0113758233891968;
        0.118539648955523,-5.40726987560716,-2.78626098093863,-0.0300373066323599,-0.0420844608434558;
        0.140643728447582,3.72899160452060,1.27299698685103,0.398039435361263,-0.0148812540681720];

elseif strcmp(ConstituentSolvents{2}, 'Urea');
    x_init = [0.000000141533896, -0.000009041287632, 0.000018598754085, 0.000000895891819, 0.000007060061838;
                -0.000009352037405, 0.000654440279566, -0.001345484994868, -0.000018847126766, -0.000334744443571;
                0.099405955673658, 41.248099258594202, 19.816928775019399, 0.405795511045443, -0.008022037545124];
 x_init = [0, 0, 0, 0, 0; 
                0, 0, 0, 0, 0; 
                0.099405955673658, 41.248099258594202, 19.816928775019399, 0.004057955110454e2, -0.008022037545124];

elseif strcmp(ConstituentSolvents{2}, 'DME');
    x_init = [-0.000000594656863, 0.000001517638887, -0.000003114239476, -0.000002001423576, -0.000000757011536;
                -0.000094496371346, -0.000282668534354, 0.000579219632818, 0.000132971648018, -0.000017219270393;
                Parameters(:,1,index), Parameters(:,2,index), Parameters(:,3,index), Parameters(:,4,index), Parameters(:,5,index)];
    x_init = [0, 0, 0, 0, 0; 
                0, 0, 0, 0, 0; 
                0.099405955673658, 41.248099258594202, 19.816928775019399, 0.004057955110454e2, -0.008022037545124];

    x_init = [-0.00913978232868119,-0.0263643358329154,0.0549357760778195,-0.0156142477338332,0.0251306174744775;
         -0.00356592613084677,-0.00326651721828416,0.00690843176071465,0.000309074424971461,-0.0281771649176793;
         0.0994029440432474,41.2496794887800,19.8176099820354,0.405563681905929,-0.00780831978687084];

elseif strcmp(ConstituentSolvents{2}, 'Diox');
%     x_init = [-0.109691135481085, 0.002124226734001, 0.001287902013219, 0.019102792855524e2, -0.000006005337946;
%                 6.570251589611317, -0.127655814324241, -0.076294924390373, -1.144265233355243e2, 0.000267817962053; 
%                 0.099405955673658, 41.248099258594202, 19.816928775019399, 0.004057955110454e2, -0.008022037545124];
    x_init = [0, 0, 0, 0, 0; 
                0, 0, 0, 0, 0; 
                0.099405955673658, 41.248099258594202, 19.816928775019399, 0.004057955110454e2, -0.008022037545124];
elseif strcmp(ConstituentSolvents{2}, 'DMF');
    x_init = [-0.000000594656863, 0.000001517638887, -0.000003114239476, -0.000002001423576, -0.000000757011536;
                -0.000094496371346, -0.000282668534354, 0.000579219632818, 0.000132971648018, -0.000017219270393;
                Parameters(:,1,index), Parameters(:,2,index), Parameters(:,3,index), Parameters(:,4,index), Parameters(:,5,index)];
    x_init = [0, 0, 0, 0, 0; 
                0, 0, 0, 0, 0; 
                0.099405955673658, 41.248099258594202, 19.816928775019399, 0.004057955110454e2, -0.008022037545124];

            
    x_init = [0.0441485456140207,0.131037075166481,-0.268718871712624,-0.0300774334143516,0.0437034119013914;
        -0.0333063259184263,-0.0499352316567967,0.103913395662729,0.0118651448867527,-0.0291679892653324;
        0.0987236900521666,41.2500654742747,19.8173055049011,0.405752055655009,-0.00782331044188380];
end

if order_toggle == 1
   x_init = x_init(2:end,:); 
end

[total_solvent_info, Parameters, Ion_Struct, Final_Diff] = optimization_script_phi_toggle(index,init,x_init,ions,ConstituentSolvents);
if init == 0
    Params_over_time = Parameters(:,:,index);
elseif init == 1
    Params_over_time = [0 0 0 0 0];
end 

if init == 1
    TSV(index) = [total_solvent_info(index).Tmin];
    TEV(index) = [total_solvent_info(index).Tmax];
    TInterval=[TSV(index),TEV(index)];
    eps_hat = ((total_solvent_info(index).eps_s(temp))-eps_in)/(total_solvent_info(index).eps_s(temp));
    eps_hat_p=((total_solvent_info(index).deps_dt(temp))*(total_solvent_info(index).eps_s(temp))...
       -(total_solvent_info(index).deps_dt(temp))*((total_solvent_info(index).eps_s(temp))-eps_in))/((total_solvent_info(index).eps_s(temp)))^2;

    Eps=total_solvent_info(index).eps_s(temp);
    dEps_dT=total_solvent_info(index).deps_dt(temp);
    deltaT = 1e-4;
    d2Eps_dT2 = (total_solvent_info(index).deps_dt(temp+deltaT)- ...
         total_solvent_info(index).deps_dt(temp-deltaT))/(2*deltaT); % for now. 
    eps_hat_p_p = (Eps^2*d2Eps_dT2 - 2*dEps_dT^2*Eps +2*(Eps-eps_in)*dEps_dT^2 ...
           - (Eps-eps_in)*d2Eps_dT2*Eps)/Eps^3;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% q=1;
% [DG_p,DS_p,En_p,dphidn_p,Sigma_p,Sigman_p]= NLBC_TANH(eps_hat,eps_hat_p,r,tol,q,total_solvent_info(index),temp,eps_in,TInterval,conv_factor,eps_hat_p_p,predict,0);  %Function for tanh
% 
% q=-1;
% [DG_n,DS_n,En_n,dphidn_n,Sigma_n,Sigman_n]= NLBC_TANH(eps_hat,eps_hat_p,r,tol,q,total_solvent_info(index),temp,eps_in,TInterval,conv_factor,eps_hat_p_p,predict,0);  %Function for tanh
% 
% dphidn_NLBC_TANH=[dphidn_p,dphidn_n(end:-1:1)];
% sigma_nom_over_dphidn_NLBC_TANH=[Sigman_p./dphidn_p,Sigman_n(end:-1:1)./dphidn_n(end:-1:1)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for j=1:length(Ion_Data);
        q=Ion_Data(j).q;
        r=Ion_Data(j).r;
        wt = 0;
        [DG_NLBC_TANH(j)]= ...                               %Function for symmetric NLBC TANH
            NLBC_TANH(wt,eps_hat,eps_hat_p,r,tol,q,total_solvent_info(index),temp,eps_in,TInterval,conv_factor,eps_hat_p_p,predict,0);
        [DG_BORN(j)]= ...                               %Function for symmetric BORN
            BORN(eps_hat,r,q,conv_factor);
    end
else
    for j=1:length(ions);
        TSV(index) = [total_solvent_info(index).Tmin];
        TEV(index) = [total_solvent_info(index).Tmax];
        TInterval=[TSV(index),TEV(index)];
        eps_hat = ((ions(j).Eps_s)-eps_in)/(ions(j).Eps_s);
        eps_hat_p=((0)*(ions(j).Eps_s)...
            -(0)*((ions(j).Eps_s)-eps_in))/((ions(j).Eps_s))^2;

        Eps=ions(j).Eps_s;
        dEps_dT= 0; % total_solvent_info(index).deps_dt(temp);
        deltaT = 1e-4;
        d2Eps_dT2 = (0- ...
            0)/(2*deltaT); % for now. 
        eps_hat_p_p = (Eps^2*d2Eps_dT2 - 2*dEps_dT^2*Eps +2*(Eps-eps_in)*dEps_dT^2 ...
            - (Eps-eps_in)*d2Eps_dT2*Eps)/Eps^3;

        q=ions(j).Charge;
        r=ions(j).Radius;
        wt = ions(j).Weight;
        [DG_NLBC_TANH(j)]= ...                               %Function for symmetric NLBC TANH
        NLBC_TANH(wt,eps_hat,eps_hat_p,r,tol,q,Params_over_time,temp,eps_in,TInterval,conv_factor,eps_hat_p_p,predict,0);
        [DG_BORN(j)]= ...                               %Function for symmetric BORN
        BORN(eps_hat,r,q,conv_factor);
    end
end
