function [total_solvent_info, Params, Ion_Struct, Final_Diff] = optimization_script_phi_toggle(index,init,x_init,ions,ConstituentSolvents)

global order_toggle phi_toggle solvent FID errorgraph 


par_num=5;
if init == 1
    solvent_opt_data=zeros(length(solvent),3*par_num);
end
num_temps=1;


if phi_toggle == 0 && init == 1 
    Params = zeros(num_temps,par_num,length(solvent));
    fprintf('%5s \n','Optimizing WITHOUT phi stat');
    fprintf('%5s \n','-----------------------------');
    fprintf('\n');
elseif init == 1
    Params = zeros(num_temps,par_num,length(solvent));
    fprintf('%5s \n','Optimizing WITH phi stat');
    fprintf('%5s \n','-----------------------------');
    fprintf('\n');
else %% Second Params is for optimization w/o optimizing the constant term
    if order_toggle == 2
        Params = zeros(3,5);
    elseif order_toggle == 1
        Params = zeros(2,5);
    end
%     Params = zeros(1,5);
    fprintf('%5s \n','Optimizing WITH phi stat');
    fprintf('%5s \n','-----------------------------');
    fprintf('\n');
end

% t=linspace(solvent(index).Tmin,solvent(index).Tmax,num_temps);
t = 25;
for j = 1:length(t)
    if init == 1
        [Params(j,:,index),Ion_Struct] = mytanhopt_temp(t(j),index,x_init,init,ions,ConstituentSolvents);
    else
        [Params(:,:),Ion_Struct,Final_Diff] = mytanhopt_temp(t(j),1,x_init,init,ions,ConstituentSolvents);
    end
end

fprintf('\n');
% for k=1:par_num
%     fitparams=polyfit(t,Params(:,k,index)',3);
%     solvent_opt_data(index,(k-1)*4+1)=fitparams(1);
%     solvent_opt_data(index,(k-1)*4+2)=fitparams(2);
%     solvent_opt_data(index,(k-1)*4+3)=fitparams(3);
%     solvent_opt_data(index,(k-1)*4+4)=fitparams(4);
% end
Input_Parameters = Params(:,:,index);
total_solvent_info(index)=create_solvent_structure(Input_Parameters,phi_toggle,index);
if phi_toggle==0 && init == 1
    fprintf(FID,['fiting parameters for static potential being excluded for ', num2str(solvent(index).Name),'\n']);
    fprintf(FID,'-------------------------------------------------------------------------\n');
    fprintf(FID,['Initial Guess: ',num2str(x_init),'\n']);
    fprintf(FID,'-------------------------------------------------------------------------\n');    
    fprintf(FID,'temp     alpha       beta       gamma          mu     phi_stat    \n');
    fprintf(FID,'-------------------------------------------------------------------------\n');    
    for kk=1:length(t)
        fprintf(FID,'%5.3f  %10.8f %10.8f %10.8f %10.8f %10.8f \n',t(kk),Params(kk,:,index),0);
    end
    fprintf(FID,'\n \n \n');
elseif phi_toggle==1 && init == 1     
    fprintf(FID,['fiting parameters including static potential for ', num2str(solvent(index).Name),'\n']);
    fprintf(FID,'-----------------------------------------------------------------------------------\n');
    fprintf(FID,['Initial Guess: ',num2str(x_init),'\n']);
    fprintf(FID,'-----------------------------------------------------------------------------------\n');    
    fprintf(FID,'temp      alpha          beta            gamma            mu          phi_stat    \n');
    fprintf(FID,'-----------------------------------------------------------------------------------\n');   
    for kk=1:length(t)
        fprintf(FID,'%5.3f    %10.8f    %10.8f    %10.8f      %10.8f      %10.8f \n',t(kk),Params(kk,:,index));
    end
    fprintf(FID,'\n \n \n');
else
    fprintf(FID,['fiting parameters for static potential being excluded for foo ','\n']);
    fprintf(FID,'-------------------------------------------------------------------------\n');
    fprintf(FID,['Initial Guess: ',mat2str(x_init),'\n']);
    fprintf(FID,'-------------------------------------------------------------------------\n');    
    fprintf(FID,'temp     alpha       beta       gamma          mu     phi_stat    \n');
    fprintf(FID,'-------------------------------------------------------------------------\n');    
    for kk=1:length(t)
        fprintf(FID,'%5.3f  %10.8f %10.8f %10.8f %10.8f %10.8f \n',t(kk),Params(kk,:,index),0);
    end
    fprintf(FID,'\n \n \n');
end
% for j=1:length(t)
%     if phi_toggle==0
%         error(j,:,index)=abs(Params(j,:,index)-[total_solvent_info(index).alpha_tanh_fit(t(j)),total_solvent_info(index).beta_tanh_fit(t(j)),total_solvent_info(index).gamma_tanh_fit(t(j)),total_solvent_info(index).mu_tanh_fit(t(j))]);
%     elseif phi_toggle==1
%         error(j,:,index)=abs(Params(j,:,index)-[total_solvent_info(index).alpha_tanh_fit(t(j)),total_solvent_info(index).beta_tanh_fit(t(j)),total_solvent_info(index).gamma_tanh_fit(t(j)),total_solvent_info(index).mu_tanh_fit(t(j)),total_solvent_info(index).phi_stat_tanh_fit(t(j))]);
%     end
% end
if errorgraph==1
    if phi_toggle==0
        figure
        plot(t,error(:,1,index),'-o','linewidth',2);
        legend('\alpha');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/O \phi_{static} ', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);
        figure
        plot(t,error(:,2,index),'-o','linewidth',2);
        legend('\beta');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/O \phi_{static} ', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);
        figure
        plot(t,error(:,3,index),'-o','linewidth',2);
        legend('\gamma');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/O \phi_{static} ', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);
        figure
        plot(t,error(:,4,index),'-o','linewidth',2);
        legend('\mu');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/O \phi_{static} ', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);

    elseif phi_toggle==1
        figure
        plot(t,error(:,1,index),'-o','linewidth',2);
        legend('\alpha');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/ \phi_{static} ', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);
        figure
        plot(t,error(:,2,index),'-o','linewidth',2);
        legend('\beta');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/ \phi_{static} ', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);
        figure
        plot(t,error(:,3,index),'-o','linewidth',2);
        legend('\gamma');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/ \phi_{static} ', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);
        figure
        plot(t,error(:,4,index),'-o','linewidth',2);
        legend('\mu');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/ \phi_{static} ', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);
        plot(t,error(:,5,index),'-o','linewidth',2);
        legend('\mu');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/ \phi_{static}', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);
    end
end

