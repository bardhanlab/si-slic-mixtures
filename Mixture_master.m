addpath('..');
addpath('Ref_figs');
addpath('export_fig');
global order_toggle plot_toggle numWeights MeOH_g_mol W_g_mol Acetone_g_mol export fill
plot_toggle = 1; % 1 for print plots 0 for supress plotting
export = 0; % Toggles the ability to save figures to a destination
fill = 0; % toggle interpolation
numWeights = 100;
order_toggle = 2; % Toggles model parameters fit order.  2 for quadratic, 1 for linear
ConstantInfo
predict = 1; % Toggles prediction.  0 is off 1 is on
if predict == 0
%% MeOH
outputFileMeOH = 'MeOH_Mixture_Solvents_All.txt';
initialFileMeOH = 'MeOH_DG_Transfer.txt';
ConstituentSolventsMeOH = {'W','MeOH'};
% Data from Suresh 02 Fig. 7 using plot digitizer
% Data only allows for a maximum mol fraction of .35 
load 'MeOH_epsilon_digitized_298K.csv'
Eps_weight_MeOH = MolToWeight(MeOH_epsilon_digitized_298K(:,1),MeOH_g_mol,W_g_mol);
EpsFun_MeOH = CalcEpsWeight(Eps_weight_MeOH,MeOH_epsilon_digitized_298K(:,2),2,0,1,100,ConstituentSolventsMeOH{2});
Water_EXP_DG = GetExpDG('Solvents.txt',ConstituentSolventsMeOH{1});

[MeOH_total_info, ~, MeOH_Ions, Wat_Ref_BORN_MeOH, Calculated_Results_MeOH, ...
    ~, ~, MeOH_Params, MeOH_Ion_Struct, ...
    ~, ~, Wat_Ref_MeOH, Fun_MeOH_Params, MeOH_Diff] = OptimizeSolvent(initialFileMeOH, ...
    EpsFun_MeOH,Ion_Struct_Reference,Water_EXP_DG,outputFileMeOH,ConstituentSolventsMeOH);
    
%% Acetone
outputFileAC = 'Acetone_Mixture_Solvents_All.txt';
initialFileAC = 'Acetone_DG_Transfer_All.txt';
ConstituentSolventsAC = {'W','AC'};
% Data from Suresh 02 Fig. 7 using plot digitizer
% Data only allows for a maximum mol fraction of .35 
load 'Acetone_epsilon_digitized_298K.csv'
Eps_weight_AC = MolToWeight(Acetone_epsilon_digitized_298K(:,1),Acetone_g_mol,W_g_mol);
EpsFun_AC = CalcEpsWeight(Eps_weight_AC,Acetone_epsilon_digitized_298K(:,2),2,0,1,100,ConstituentSolventsAC{2});

[AC_total_info, ~, AC_Ions, Wat_Ref_BORN_AC, Calculated_Results_AC, ...
    ~, ~, AC_Params, AC_Ion_Struct, ...
    ~, ~, Wat_Ref_AC, Fun_AC_Params, AC_Diff] = OptimizeSolvent(initialFileAC, ...
    EpsFun_AC,Ion_Struct_Reference,Water_EXP_DG,outputFileAC,ConstituentSolventsAC);

%% DMSO
outputFileDMSO = 'DMSO_Mixture_Solvents_All.txt';
initialFileDMSO = 'DMSO_DG_Transfer_All.txt';
ConstituentSolventsDMSO = {'W','DMSO'};
% Data from Markarianet al
DMSO_Concentrations = [0 0.0504 .101 .249 .402 .497 .602 .706 .899 1];
DMSO_Eps = [78.36 76.59 76.22 72.87 67.64 64.11 60.23 56.24 49.44 46.20];
[~,EpsFun_DMSO] = SolventEps(DMSO_Concentrations,DMSO_Eps,2);

[DMSO_total_info, ~, DMSO_Ions, Wat_Ref_BORN_DMSO, Calculated_Results_DMSO, ...
    ~, ~, DMSO_Params, DMSO_Ion_Struct, ...
    ~, ~, Wat_Ref_DMSO, Fun_DMSO_Params, DMSO_Diff] = OptimizeSolvent(initialFileDMSO, ...
    EpsFun_DMSO,Ion_Struct_Reference,Water_EXP_DG,outputFileDMSO,ConstituentSolventsDMSO);

%% Ethanol
outputFileEtOH = 'Ethanol_Mixture_Solvents_All.txt';
initialFileEtOH = 'EtOH_DG_Transfer_All.txt';
ConstituentSolventsEtOH = {'W','EtOH'};
% Data from Akerlof32 Table 12
EtOH_Concentrations = linspace(0,1,11);
EtOH_Eps = [78.5 72.8 67.0 61.1 55.0 49.0 43.4 38.0 32.8 28.1 24.3];
[~,EpsFun_EtOH] = SolventEps(EtOH_Concentrations,EtOH_Eps,2);

[EtOH_total_info, ~, EtOH_Ions, Wat_Ref_BORN_EtOH, Calculated_Results_EtOH, ...
    ~, ~, EtOH_Params, EtOH_Ion_Struct, ...
    ~, ~, Wat_Ref_EtOH, Fun_EtOH_Params, EtOH_Diff] = OptimizeSolvent(initialFileEtOH, ...
    EpsFun_EtOH,Ion_Struct_Reference,Water_EXP_DG,outputFileEtOH,ConstituentSolventsEtOH);

%% AN
outputFileAN = 'AN_Mixture_Solvents_All.txt';
initialFileAN = 'AN_DG_Transfer_All.txt';
ConstituentSolventsAN = {'W','AN'};
% Data from Roy et all
AN_Vol_Vol = [0 30 60 75];
AN_Eps = [78.48 65.5 51.0 44.72];
Eps_weight_AN = (VolToWeight(AN_Vol_Vol,Acetonitrile_g_ml,W_g_ml));
[~,EpsFun_AN] = SolventEps(Eps_weight_AN,AN_Eps,2);

[AN_total_info, ~, AN_Ions, Wat_Ref_BORN_AN, Calculated_Results_AN, ...
    ~, ~, AN_Params, AN_Ion_Struct, ...
    ~, ~, Wat_Ref_AN, Fun_AN_Params, AN_Diff] = OptimizeSolvent(initialFileAN, ...
    EpsFun_AN,Ion_Struct_Reference,Water_EXP_DG,outputFileAN,ConstituentSolventsAN);

%% Urea
outputFileUrea = 'Urea_Mixture_Solvents_All.txt';
initialFileUrea = 'Urea_DG_Transfer_All.txt';
ConstituentSolventsUrea = {'W','Urea'};
% Data from Wyman J. Jr.
Urea_Concentrations = [0 11.52 20.31 29.64 36.83 42.47]./100;
Urea_Eps = [78.54 83.90 87.95 91.76 94.43 96.58];
[~,EpsFun_Urea] = SolventEps(Urea_Concentrations,Urea_Eps,2);

[Urea_total_info, ~, Urea_Ions, Wat_Ref_BORN_Urea, Calculated_Results_Urea, ...
    ~, ~, Urea_Params, Urea_Ion_Struct, ...
    ~, ~, Wat_Ref_Urea, Fun_Urea_Params, Urea_Diff] = OptimizeSolvent(initialFileUrea, ...
    EpsFun_Urea,Ion_Struct_Reference,Water_EXP_DG,outputFileUrea,ConstituentSolventsUrea);

%% DME
outputFileDME = 'DME_Mixture_Solvents_All.txt';
initialFileDME = 'DME_DG_Transfer_All.txt';
ConstituentSolventsDME = {'W','DME'};
% Data from Douheret et all
DME_Mol_Frac = [0 .0086 0.155 .0262 .0339 .0495 .0626 .0743 .0862 .1063 .1173 .1499 .18 .2194 .2722 .333 .3991 .4719 .5337 .6021 .6701 .7279 .7852 .8521 .9022 .9492 1];
DME_Eps = [78.35 76.21 74.56 72.05 70.66 67.22 64.44 62.49 60.39 57.43 55.27 50.62 46.82 42.58 38.02 33.52 29.85 26.46 24.02 21.79 19.86 18.46 17.23 15.92 14.99 14.19 13.38];
Eps_weight_DME = (MolToWeight(DME_Mol_Frac,DME_g_mol,W_g_mol));
[~,EpsFun_DME] = SolventEps(Eps_weight_DME,DME_Eps,2);

[DME_total_info, ~, DME_Ions, Wat_Ref_BORN_DME, Calculated_Results_DME, ...
    ~, ~, DME_Params, DME_Ion_Struct, ...
    ~, ~, Wat_Ref_DME, Fun_DME_Params, DME_Diff] = OptimizeSolvent(initialFileDME, ...
    EpsFun_DME,Ion_Struct_Reference,Water_EXP_DG,outputFileDME,ConstituentSolventsDME);

%% Diox
outputFileDiox = 'Diox_Mixture_Solvents_All.txt';
initialFileDiox = 'Diox_DG_Transfer_All.txt';
ConstituentSolventsDiox = {'W','Diox'};
% Data from Akerlof and Oliver A Short
EpsFun_Diox = ComputingDioxEps();

[Diox_total_info, ~, Diox_Ions, Wat_Ref_BORN_Diox, Calculated_Results_Diox, ...
    ~, ~, Diox_Params, Diox_Ion_Struct, ...
    ~, ~, Wat_Ref_Diox, Fun_Diox_Params, Diox_Diff] = OptimizeSolvent(initialFileDiox, ...
    EpsFun_Diox,Ion_Struct_Reference,Water_EXP_DG,outputFileDiox,ConstituentSolventsDiox);

%% DMF
outputFileDMF = 'DMF_Mixture_Solvents_All.txt';
initialFileDMF = 'DMF_DG_Transfer_All.txt';
ConstituentSolventsDMF = {'W','DMF'};
% Data from Kumbharkhane et al
DMF_Vol_Vol = [0 5 linspace(10,90,9) 95 100];
DMF_Eps = [79.5 77.89 76.79 75.75 74.97 72.12 67.9 63.09 57 51.87 46.21 41.94 39.88];
Eps_weight_DMF = (VolToWeight(DMF_Vol_Vol,DMF_g_ml,W_g_ml));
[~,EpsFun_DMF] = SolventEps(Eps_weight_DMF,DMF_Eps,2);

[DMF_total_info, ~, DMF_Ions, Wat_Ref_BORN_DMF, Calculated_Results_DMF, ...
    ~, ~, DMF_Params, DMF_Ion_Struct, ...
    ~, ~, Wat_Ref_DMF, Fun_DMF_Params, DMF_Diff] = OptimizeSolvent(initialFileDMF, ...
    EpsFun_DMF,Ion_Struct_Reference,Water_EXP_DG,outputFileDMF,ConstituentSolventsDMF);

end 
%Predictions
if predict == 1
    if order_toggle == 1 && fill == 0
        path = 'Output/Global_Fit_Concentrations_0-1_Linear';
        file = 'Output/Global_Fit_Concentrations_0-1_Linear/';
    elseif order_toggle == 1 && fill == 1
        path = 'Output/Global_Fit_Concentrations_0-1_Linear_filled';
        file = 'Output/Global_Fit_Concentrations_0-1_Linear_filled/';
    elseif order_toggle == 2 && fill == 0
        path = 'Output/Global_Fit_Concentrations_0-1';
        file = 'Output/Global_Fit_Concentrations_0-1/';
    elseif order_toggle == 2 && fill == 1
        path = 'Output/Global_Fit_Concentrations0-1_Quad_fill';
        file = 'Output/Global_Fit_Concentrations0-1_Quad_fill/';
    end
%     [Pred_MeOH, Conc_MeOH, Pred_BORN_MeOH, MeOH_Born_Radius, MeOH_Ref] = predictDGTrans(MeOH_total_info,EpsFun_MeOH,Fun_MeOH_Params,{'40MeOH','50MeOH'},(0:2:60)/100,ConstituentSolventsMeOH,Wat_Ref_MeOH,MeOH_Ions,Wat_Ref_BORN_MeOH,Ion_Struct_Reference);
    [Pred_MeOH, Conc_MeOH, Pred_BORN_MeOH, MeOH_Born_Radius, MeOH_Ref] = predictDGTransVarriedConcentration(MeOH_total_info,EpsFun_MeOH,Fun_MeOH_Params,{'40MeOH','50MeOH'},Wat_Ref_MeOH,MeOH_Ions,Wat_Ref_BORN_MeOH,Ion_Struct_Reference,path,file);
    %     writeResults2('Output/Methanol_Predict.tex',MeOH_Ions,Pred_MeOH,ConstituentSolventsMeOH,predict,'specific',Conc_MeOH);
%     [Pred_AC, Conc_AC, Pred_BORN_AC, AC_Born_Radius, AC_Ref] = predictDGTrans(AC_total_info,EpsFun_AC,Fun_AC_Params,{'40AC','50AC'},(0:2:60)/100,ConstituentSolventsAC,Wat_Ref_AC,AC_Ions,Wat_Ref_BORN_AC,Ion_Struct_Reference);
    [Pred_AC, Conc_AC, Pred_BORN_AC, AC_Born_Radius, AC_Ref] = predictDGTransVarriedConcentration(AC_total_info,EpsFun_AC,Fun_AC_Params,{'40AC','50AC'},Wat_Ref_AC,AC_Ions,Wat_Ref_BORN_AC,Ion_Struct_Reference,path,file);
%     writeResults2('Output/Acetone_Predict.tex',AC_Ions,Pred_AC,ConstituentSolventsAC,predict,'specific',Conc_AC);
%     [Pred_DMSO, Conc_DMSO, Pred_BORN_DMSO, DMSO_Born_Radius, DMSO_Ref] = predictDGTrans(DMSO_total_info,EpsFun_DMSO,Fun_DMSO_Params,{'40DMSO','50DMSO'},(0:2:60)/100,ConstituentSolventsDMSO,Wat_Ref_DMSO,DMSO_Ions,Wat_Ref_BORN_DMSO,Ion_Struct_Reference);
    [Pred_DMSO, Conc_DMSO, Pred_BORN_DMSO, DMSO_Born_Radius, DMSO_Ref] = predictDGTransVarriedConcentration(DMSO_total_info,EpsFun_DMSO,Fun_DMSO_Params,{'40DMSO','50DMSO'},Wat_Ref_DMSO,DMSO_Ions,Wat_Ref_BORN_DMSO,Ion_Struct_Reference,path,file);
%     writeResults2('Output/DMSO_Predict.tex',DMSO_Ions,Pred_DMSO,ConstituentSolventsDMSO,predict,'specific',Conc_DMSO);
%     [Pred_EtOH, Conc_EtOH, Pred_BORN_EtOH, EtOH_Born_Radius, EtOH_Ref] = predictDGTrans(EtOH_total_info,EpsFun_EtOH,Fun_EtOH_Params,{'40EtOH','50EtOH'},(0:2:60)/100,ConstituentSolventsEtOH,Wat_Ref_EtOH,EtOH_Ions,Wat_Ref_BORN_EtOH,Ion_Struct_Reference);
    [Pred_EtOH, Conc_EtOH, Pred_BORN_EtOH, EtOH_Born_Radius, EtOH_Ref] = predictDGTransVarriedConcentration(EtOH_total_info,EpsFun_EtOH,Fun_EtOH_Params,{'40EtOH','50EtOH'},Wat_Ref_EtOH,EtOH_Ions,Wat_Ref_BORN_EtOH,Ion_Struct_Reference,path,file);
%     writeResults2('Output/Ethanol_Predict.tex',EtOH_Ions,Pred_EtOH,ConstituentSolventsEtOH,predict,'specific',Conc_EtOH);
%     [Pred_AN, Conc_AN, Pred_BORN_AN, AN_Born_Radius, AN_Ref] = predictDGTrans(AN_total_info,EpsFun_AN,Fun_AN_Params,{'40AN','50AN'},(0:2:60)/100,ConstituentSolventsAN,Wat_Ref_AN,AN_Ions,Wat_Ref_BORN_AN,Ion_Struct_Reference);
    [Pred_AN, Conc_AN, Pred_BORN_AN, AN_Born_Radius, AN_Ref] = predictDGTransVarriedConcentration(AN_total_info,EpsFun_AN,Fun_AN_Params,{'40AN','50AN'},Wat_Ref_AN,AN_Ions,Wat_Ref_BORN_AN,Ion_Struct_Reference,path,file);
%     writeResults2('Output/AN_Predict.tex',AN_Ions,Pred_AN,ConstituentSolventsAN,predict,'specific',Conc_AN);
%     [Pred_Urea, Conc_Urea, Pred_BORN_Urea, Urea_Born_Radius, Urea_Ref] = predictDGTrans(Urea_total_info,EpsFun_Urea,Fun_Urea_Params,{'40Urea','50Urea'},(0:2:60)/100,ConstituentSolventsUrea,Wat_Ref_Urea,Urea_Ions,Wat_Ref_BORN_Urea,Ion_Struct_Reference);
    [Pred_Urea, Conc_Urea, Pred_BORN_Urea, Urea_Born_Radius, Urea_Ref] = predictDGTransVarriedConcentration(Urea_total_info,EpsFun_Urea,Fun_Urea_Params,{'40Urea','50Urea'},Wat_Ref_Urea,Urea_Ions,Wat_Ref_BORN_Urea,Ion_Struct_Reference,path,file);
%     writeResults2('Output/Urea_Predict.tex',Urea_Ions,Pred_Urea,ConstituentSolventsUrea,predict,'specific',Conc_Urea);
%     [Pred_DME, Conc_DME, Pred_BORN_DME, DME_Born_Radius, DME_Ref] = predictDGTrans(DME_total_info,EpsFun_DME,Fun_DME_Params,{'40DME','50DME'},(0:2:60)/100,ConstituentSolventsDME,Wat_Ref_DME,DME_Ions,Wat_Ref_BORN_DME,Ion_Struct_Reference);
    [Pred_DME, Conc_DME, Pred_BORN_DME, DME_Born_Radius, DME_Ref] = predictDGTransVarriedConcentration(DME_total_info,EpsFun_DME,Fun_DME_Params,{'40DME','50DME'},Wat_Ref_DME,DME_Ions,Wat_Ref_BORN_DME,Ion_Struct_Reference,path,file);
%     writeResults2('Output/DME_Predict.tex',DME_Ions,Pred_DME,ConstituentSolventsDME,predict,'specific',Conc_DME);
%     [Pred_Diox, Conc_Diox, Pred_BORN_Diox, Diox_Born_Radius, Diox_Ref] = predictDGTrans(Diox_total_info,EpsFun_Diox,Fun_Diox_Params,{'40Diox','50Diox'},(0:2:60)/100,ConstituentSolventsDiox,Wat_Ref_Diox,Diox_Ions,Wat_Ref_BORN_Diox,Ion_Struct_Reference);
    [Pred_Diox, Conc_Diox, Pred_BORN_Diox, Diox_Born_Radius, Diox_Ref] = predictDGTransVarriedConcentration(Diox_total_info,EpsFun_Diox,Fun_Diox_Params,{'40Diox','50Diox'},Wat_Ref_Diox,Diox_Ions,Wat_Ref_BORN_Diox,Ion_Struct_Reference,path,file);
%     writeResults2('Output/Diox_Predict.tex',Diox_Ions,Pred_Diox,ConstituentSolventsDiox,predict,'specific',Conc_Diox);
%     [Pred_DMF, Conc_DMF, Pred_BORN_DMF, DMF_Born_Radius, DMF_Ref] = predictDGTrans(DMF_total_info,EpsFun_DMF,Fun_DMF_Params,{'40DMF','50DMF'},(0:2:60)/100,ConstituentSolventsDMF,Wat_Ref_DMF,DMF_Ions,Wat_Ref_BORN_DMF,Ion_Struct_Reference);
    [Pred_DMF, Conc_DMF, Pred_BORN_DMF, DMF_Born_Radius, DMF_Ref] = predictDGTransVarriedConcentration(DMF_total_info,EpsFun_DMF,Fun_DMF_Params,{'40DMF','50DMF'},Wat_Ref_DMF,DMF_Ions,Wat_Ref_BORN_DMF,Ion_Struct_Reference,path,file);
%     writeResults2('Output/Diox_Predict.tex',DMF_Ions,Pred_DMF,ConstituentSolventsDMF,predict,'specific',Conc_DMF);

[Ref_neat_Water,DG_solv_table] = createNeatWaterTable({MeOH_Params,DMSO_Params,AC_Params,EtOH_Params,AN_Params,Urea_Params,DME_Params,Diox_Params,DMF_Params},{EpsFun_MeOH,EpsFun_DMSO,EpsFun_AC,EpsFun_EtOH,EpsFun_AN,EpsFun_Urea,EpsFun_DME,EpsFun_Diox,EpsFun_DMF},Ion_Struct_Reference,{'MeOH', 'DMSO', 'AC', 'EtOH', 'AN', 'Urea', 'DME', 'Diox', 'DMF'},file);

MeOH_Error = calcError([-529 -424 -352 -329 -306  -304 -278 -243],table2struct(Ref_neat_Water),MeOH_total_info,EpsFun_MeOH, MeOH_Params);
AC_Error = calcError([-529 -424 -352 -329 -306  -304 -278 -243],table2struct(Ref_neat_Water),AC_total_info,EpsFun_AC, AC_Params);
DMSO_Error = calcError([-529 -424 -352 -329 -306  -304 -278 -243],table2struct(Ref_neat_Water),DMSO_total_info,EpsFun_DMSO, DMSO_Params);
EtOH_Error = calcError([-529 -424 -352 -329 -306  -304 -278 -243],table2struct(Ref_neat_Water),EtOH_total_info,EpsFun_EtOH, EtOH_Params);
AN_Error = calcError([-529 -424 -352 -329 -306  -304 -278 -243],table2struct(Ref_neat_Water),AN_total_info,EpsFun_AN, AN_Params);
Urea_Error = calcError([-529 -424 -352 -329 -306  -304 -278 -243],table2struct(Ref_neat_Water),Urea_total_info,EpsFun_Urea, Urea_Params);
DME_Error = calcError([-529 -424 -352 -329 -306  -304 -278 -243],table2struct(Ref_neat_Water),DME_total_info,EpsFun_DME, DME_Params);
Diox_Error = calcError([-529 -424 -352 -329 -306  -304 -278 -243],table2struct(Ref_neat_Water),Diox_total_info,EpsFun_Diox, Diox_Params);
DMF_Error = calcError([-529 -424 -352 -329 -306  -304 -278 -243],table2struct(Ref_neat_Water),DMF_total_info,EpsFun_DMF, DMF_Params);
PredictIonsNotPresent({EpsFun_MeOH,EpsFun_AC,EpsFun_DMSO,EpsFun_EtOH,EpsFun_AN,EpsFun_Urea,EpsFun_DME,EpsFun_Diox,EpsFun_DMF},{MeOH_Params,AC_Params,DMSO_Params,EtOH_Params,AN_Params,Urea_Params,DME_Params,Diox_Params,DMF_Params},{MeOH_total_info,AC_total_info,DMSO_total_info,EtOH_total_info,AN_total_info,Urea_total_info,DME_total_info,Diox_total_info,DMF_total_info},(0:2:60)/100,{'MeOH','AC','DMSO','EtOH','AN','Urea','DME','Diox','DMF'},Ion_Struct_Reference2);

processErrors
end


if plot_toggle == 1 && predict == 1
    if order_toggle == 1 && fill == 0
        path = 'Output/Global_Fit_Concentrations_0-1_Linear';
    elseif order_toggle == 1 && fill == 1
        path = 'Output/Global_Fit_Concentrations_0-1_Linear_filled';
    elseif order_toggle == 2 && fill == 0
        path = 'Output/Global_Fit_Concentrations_0-1';
    elseif order_toggle == 2 && fill == 1
        path = 'Output/Global_Fit_Concentrations0-1_Quad_fill';
    end
    x = 0:.05:1;    
    getPlotResults_Glob(MeOH_total_info,Pred_MeOH,Conc_MeOH,MeOH_Ions,ConstituentSolventsMeOH,Pred_BORN_MeOH,Concentrations,path)
    figure
    getPlotResults_Glob(AC_total_info,Pred_AC,Conc_AC,AC_Ions,ConstituentSolventsAC,Pred_BORN_AC,Concentrations,path)
    figure
    getPlotResults_Glob(DMSO_total_info,Pred_DMSO,Conc_DMSO,DMSO_Ions,ConstituentSolventsDMSO,Pred_BORN_DMSO,Concentrations,path)
    figure
    getPlotResults_Glob(EtOH_total_info,Pred_EtOH,Conc_EtOH,EtOH_Ions,ConstituentSolventsEtOH,Pred_BORN_EtOH,Concentrations,path)
    figure
    getPlotResults_Glob(AN_total_info,Pred_AN,Conc_AN,AN_Ions,ConstituentSolventsAN,Pred_BORN_AN,Concentrations,path)
    figure
    getPlotResults_Glob(Urea_total_info,Pred_Urea,Conc_Urea,Urea_Ions,ConstituentSolventsUrea,Pred_BORN_Urea,Concentrations,path)
    figure
    getPlotResults_Glob(DME_total_info,Pred_DME,Conc_DME,DME_Ions,ConstituentSolventsDME,Pred_BORN_DME,Concentrations,path)
    figure
    getPlotResults_Glob(Diox_total_info,Pred_Diox,Conc_Diox,Diox_Ions,ConstituentSolventsDiox,Pred_BORN_Diox,Concentrations,path)
    figure
    getPlotResults_Glob(DMF_total_info,Pred_DMF,Conc_DMF,DMF_Ions,ConstituentSolventsDMF,Pred_BORN_DMF,Concentrations,path)
    figure
    plot_phi(x,Fun_MeOH_Params(5).Functions,ConstituentSolventsMeOH)
    figure
    plot_phi(x,Fun_AC_Params(5).Functions,ConstituentSolventsAC)
    figure
    plot_phi(x,Fun_AN_Params(5).Functions,ConstituentSolventsAN)
    figure
    plot_phi(x,Fun_Diox_Params(5).Functions,ConstituentSolventsDiox)
    figure
    plot_phi(x,Fun_DME_Params(5).Functions,ConstituentSolventsDME)
    figure
    plot_phi(x,Fun_DMF_Params(5).Functions,ConstituentSolventsDMF)
    figure
    plot_phi(x,Fun_DMSO_Params(5).Functions,ConstituentSolventsDMSO)
    figure
    plot_phi(x,Fun_EtOH_Params(5).Functions,ConstituentSolventsEtOH)
    figure
    plot_phi(x,Fun_Urea_Params(5).Functions,ConstituentSolventsUrea)
    figure
%     PlotTotalSolvation({EpsFun_MeOH,EpsFun_AC,EpsFun_DMSO,EpsFun_EtOH,EpsFun_AN,EpsFun_Urea,EpsFun_DME,EpsFun_Diox,EpsFun_DMF},{MeOH_Params,AC_Params,DMSO_Params,EtOH_Params,AN_Params,Urea_Params,DME_Params,Diox_Params,DMF_Params},{MeOH_total_info,AC_total_info,DMSO_total_info,EtOH_total_info,AN_total_info,Urea_total_info,DME_total_info,Diox_total_info,DMF_total_info},(0:2:60)/100,{'MeOH','AC','DMSO','EtOH','AN','Urea','DME','Diox','DMF'},Ion_Struct_Reference);
%     PlotCationsAnions({'Output/DGCations','Output/DGAnions'},{EpsFun_MeOH,EpsFun_AC,EpsFun_DMSO,EpsFun_EtOH,EpsFun_AN,EpsFun_Urea,EpsFun_DME,EpsFun_Diox,EpsFun_DMF},{MeOH_Params,AC_Params,DMSO_Params,EtOH_Params,AN_Params,Urea_Params,DME_Params,Diox_Params,DMF_Params},{MeOH_total_info,AC_total_info,DMSO_total_info,EtOH_total_info,AN_total_info,Urea_total_info,DME_total_info,Diox_total_info,DMF_total_info},(0:2:60)/100,{'MeOH','AC','DMSO','EtOH','AN','Urea','DME','Diox','DMF'},Ion_Struct_Reference);
    PlotCationsAnions2({[path,'/DGCations'],[path,'/DGAnions']},{EpsFun_MeOH,EpsFun_AC,EpsFun_DMSO,EpsFun_EtOH,EpsFun_AN,EpsFun_Urea,EpsFun_DME,EpsFun_Diox,EpsFun_DMF},{MeOH_Params,AC_Params,DMSO_Params,EtOH_Params,AN_Params,Urea_Params,DME_Params,Diox_Params,DMF_Params},{MeOH_total_info,AC_total_info,DMSO_total_info,EtOH_total_info,AN_total_info,Urea_total_info,DME_total_info,Diox_total_info,DMF_total_info},{'MeOH','AC','DMSO','EtOH','AN','Urea','DME','Diox','DMF'},Ion_Struct_Reference);

end

