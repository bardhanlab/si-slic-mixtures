% Constants for mixture master
% Input the grams per mol values for any constituent solvent in question
phi_toggle = 1;
MeOH_g_mol = 32.04186;
W_g_mol = 18.01528;
W_g_ml = 1.0040;
Acetone_g_mol = 58.07914;
Glycerol_g_mol = 92.09382;
Acetonitrile_g_ml = .786;
DME_g_mol = 90.12;
DMF_g_ml = 0.948;
weight_low = .1;
weight_high = .6;
weight_step = .05;
Concentrations = weight_low:weight_step:weight_high;
Li=struct('Name','Li','r',0.88,'q',1,'color',1 );
Na=struct('Name','Na','r',1.16,'q',1,'color',2 );
K =struct('Name','K' ,'r',1.52,'q',1,'color',3 );
Rb=struct('Name','Rb','r',1.63,'q',1,'color',4 );
Cs=struct('Name','Cs','r',1.84,'q',1,'color',5 );
F =struct('Name','F' ,'r',1.19,'q',-1,'color',6);
Cl=struct('Name','Cl','r',1.67,'q',-1,'color',7);
Br=struct('Name','Br','r',1.82,'q',-1,'color',8);
I =struct('Name','I' ,'r',2.06,'q',-1,'color',9);
Ion_Struct_Reference =[Li,Na,K,Rb,Cs,Cl,Br,I];
Ion_Struct_Reference2 = [Li,Na,K,Rb,Cs,F,Cl,Br,I];
