In order to reproduce the results published please follow these steps:

To run from scratch:

1) Select the type of optimization scheme you would like to run.  To do this, there are 2 important toggles.  The first is the fill toggle and the second is the order_toggle.
   a) If you would like to have quadratically varying model parameters, set order_toggle to 2.
   b) For linearly varying parameters, set order_toggle to 1.

2) With the proper order_toggle set, you can elect to add more data to the optimization via interpolation.
   a) Due to the limited data in some cases, one can extrapolate additional 'experimental' data points by setting fill to 1.
   b) To run the optimization with strictly tabulated data, set fill to 0.

3) To run the optimization after all toggles have been set, run the code Mixture_master.m

4) After the optimization is completed, to generate the plots and tables you can set the plot_toggle and export toggle.
   a) To print out the plots to the screen, set plot_toggle to 1.
   b) To only save variable and not print out plots to screen, set plot_toggle to 0.

5) In order to save the plots and tables to the different directories in the repo, set export to 1.  To simply see the plots on the screen, set export to 0.

6) Once all of the toggles are set to the desired positions, set the predict toggle to 1 and run Mixture_master.m again.  This will bypass the optimization loop and simply plot and predict all of the results.

To run from pre-loaded data

1) In the repo there are 4 *.mat files.  They are labeled Quad_No_Fill, Quadratic_Fill, Linear_No_Fill, and Linear_Fill.  These files house all of the variables needed to plot and obtain the results presented in the paper without optimization.  To do so, simply load whichever results you would like to see and run the code Mixture_master.m with predict set to 1.

NOTE: In some older versions of MATLAB, some functions necessary in the plotting loop are not stored correctly and as such will generate an error hen trying to run the code from pre-loaded data.  If this is the case, one must run the optimization loop with the desired toggles set.

Output:

If export was set to 1 during any of the runs, there will be data output to certain directories in your file system.  These paths have been pre-defined and already set up inside of the repo.  Depending on the type of run, data will be output to the Output/<run specific folder>.  

Note: If running on a windows machine, there may be an error using export_fig.  This error could possibly come from ghostscript not being installed on your machine.  To install this, follow this link: http://www.ghostscript.com
