function plot_phi(Conc, Fun, ConstituentSolvents)

plot(Conc,Fun(Conc));
xlabel(['Concentraion of ',ConstituentSolvents{2}]);
ylabel('\phi_{stat}');
title(['\Phi_{stat} vs. Concentration in ',ConstituentSolvents{2}]);