function [ion_ref,index] = removeIons(IonData,ions)

for i = 1:length(IonData)
    IonNames{i} = IonData(i).Name;
end
j = 1;
for i = 1:length(ions)
    [m,index(i)] = ismember(ions(i).Ion,IonNames);
    if m
       ion_ref(j) = IonData(index(i));
       j = j+1;
    end
end
