function [final, Err_Save] =  amsa_temp_glob(x,tx,Ref_energy,index,ions, init)
global phi_toggle eps_in order_toggle

% Ions
global Ion_Data
alpha_vect = x(:,1);
beta_vect = x(:,2);
gamma_vect = x(:,3);
mu_vect = x(:,4);
phi_vect = x(:,5);
for i = 1:length(ions)
    [alpha, beta, gamma, mu, phistat] = Calc_Params(ions(i).Weight,alpha_vect,beta_vect,gamma_vect, ...
    mu_vect,phi_vect);
    
    eps_hat = (((ions(i).Eps_s)-eps_in)/(ions(i).Eps_s));
    dGdN = -1./(ions(i).Radius).^2;
    dphidn_NLBC = -(ions(i).Charge)./(ions(i).Radius).^2;
    sigma_0 = eps_hat*ions(i).Charge*dGdN;
    ERROR = 1e10;
    if order_toggle == 2
        Error_target = 1e-10;
    elseif order_toggle == 1
        Error_target = 1e-9;
    end
    counter = 1;
    while ERROR > Error_target
        sigma = sigma_0;
        sigma_save(counter) = sigma;
        E_n = -(ions(i).Charge)*dGdN-(-0.5)*sigma; 
        En_save(counter) = E_n;
        hf = alpha*tanh(beta*(E_n)-gamma)+mu;
        hf_save(counter) = hf;
        sigma_0 = eps_hat*ions(i).Charge*dGdN./(1+hf);
        ERROR1 = max(abs((sigma-sigma_0)./sigma_0));
        ERROR2 = max(abs(sigma-sigma_0));
        ERROR = max(ERROR1,ERROR2);
        Err_Save(counter) = ERROR;
        counter = counter + 1;
        if counter >= 1000000
            keyboard
%             Eps_S = ions(i).Eps_s;
%             Charge = ions(i).Charge;
%             Radius = ions(i).Radius;
%             save('params.mat','x');
%             save('Eps_s.mat','Eps_S');
%             save('Q.mat','Charge');
%             save('R.mat','Radius');
%             save('ions.mat','ions');
%             save('Ref.mat','Ref_energy');
            
        end
    end        
    sigma = sigma_0;
    DG(i) = 0.5*ions(i).Radius*ions(i).Charge*sigma+ions(i).Charge*phistat;
end
% diff = sum((DG(:,index)-Ref_energy(:,index)).^2);
if init == 1 || init == 0
    final = DG-Ref_energy;
else
    temp = DG-Ref_energy;
    final = sum(DG.^2-Ref_energy.^2);
end
% final = transpose(final);
% final = sum(diff);
foo = 0;