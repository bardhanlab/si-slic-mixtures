function getPlotResults_Glob(Experiment,Predicted,PredictedWeights,Ion_struct,ConstituentSolvents,BORN_Results,Concentrations,path)

global export
% Allowing to handle the concentrations in differnet forms.
if length(PredictedWeights(:,1)) > 1
    matform = 1;
else
    matform = 0;
end
varname = inputname(1);
Str = strsplit(varname,'_');
Solvent = Str{1};


for j = 1:length(Predicted(1,:))
    index = find(strcmp(Ion_struct(j).Ion,{Experiment.Ion}));
    Exp_y = [Experiment(index).DG_Trans_Exp];
    Exp_x = [Experiment(index).Weight];
    plot(Exp_x,Exp_y,'bsquare','Markersize',15,'MarkerFaceColor','b');
    hold on
    if matform
        plot(PredictedWeights(j,:),Predicted(:,j),'k--','linewidth',3)
        plot(PredictedWeights(j,:),BORN_Results(:,j),'r--','linewidth',3)
    else
        plot(PredictedWeights,Predicted(:,j),'k--','linewidth',3)
        plot(PredictedWeights,BORN_Results(:,j),'r--','linewidth',3)
    end
    xlabel(['Weight Weight Percentage of ', ConstituentSolvents{2}],'FontSize',20);
    ylabel('\Delta G_{tr}^{\circ}','FontSize',20);
    title(['\Delta G_{tr_{exp}}^{\circ} of several different models ',ConstituentSolvents{2},' for ',Ion_struct(j).Ion],'FontSize',20); 
    set(gca,'Fontsize',20);
    %     if strcmp(Solvent,'MeOH') || strcmp(Solvent,'AC') || strcmp(Solvent,'EtOH')
%         legend('Experimental','Predicted','Born','Location','Northwest');  
%     elseif (strcmp(Solvent,'AN') || strcmp(Solvent,'Urea') || strcmp(Solvent,'DMSO') || strcmp(Solvent,'DME') || strcmp(Solvent,'DMF')) && j <= 5
%         legend('Experimental','Predicted','Born','Location','Southwest');    
%     elseif (strcmp(Solvent,'AN') || strcmp(Solvent,'Urea') || strcmp(Solvent,'DMSO') || strcmp(Solvent,'DME') || strcmp(Solvent,'DMF')) && j > 5
%         legend('Experimental','Predicted','Born','Location','Northwest');    
%     elseif strcmp(Solvent,'Diox')
%         legend('Experimental','Predicted','Born','Location','Northwest');   
%     end
% 
%     set(legend,'Fontsize',20);
    if export == 1
       filename = sprintf([path,'/%s.pdf'],[ConstituentSolvents{2},'_',Ion_struct(j).Ion]);
       export_fig(filename,'-painters','-transparent');
    end
    if j ~= length(Predicted(1,:))
        figure
    end
end
