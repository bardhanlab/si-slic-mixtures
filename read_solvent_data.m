function [solvent, Table] = read_solvent_data(file,solvent_type,ConstituentSolvents)

Table_temp = readtable(file,'Delimiter',',');
solvent_temp = table2struct(Table_temp);
numrows = length(solvent_temp);

for i = 1:numrows
    eps_s_fun = str2func(solvent_temp(i).eps_s);
    solvent_temp(i).eps_s = eps_s_fun;
    deps_fun = str2func(solvent_temp(i).deps_dt);
    solvent_temp(i).deps_dt = deps_fun;
    EXP_G_Array = str2num(solvent_temp(i).EXP_G);
    solvent_temp(i).EXP_G = EXP_G_Array;
    EXP_S_Array = str2num(solvent_temp(i).EXP_S);
    solvent_temp(i).EXP_S = EXP_S_Array;
    EXP_HEATCAP_Array = str2num(solvent_temp(i).EXP_HEATCAP);
    solvent_temp(i).EXP_HEATCAP = EXP_HEATCAP_Array; 
end



for i = 1:length(solvent_type)
    for j = 1:numrows
        if strcmp(solvent_type(i),solvent_temp(j).Name)
            solvent(i) = solvent_temp(j);
            break
        end
    end

end

for i = 1:length(solvent_temp)
    SolventNames{i} = solvent_temp(i).Name;
end


if strcmp(solvent_type,'all') & ismember(ConstituentSolvents{2},SolventNames)
    solvent = solvent_temp(3:end);
elseif strcmp(solvent_type,'all') & not(ismember(ConstituentSolvents{2},SolventNames))
    solvent = solvent_temp(2:end);
end


solvent = solvent';
Table = struct2table(solvent);

