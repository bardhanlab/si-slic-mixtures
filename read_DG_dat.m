function [data, Table, ions] = read_DG_dat(infile,Eps_Fun,Ion_info,Ref_DG,outfile, ...
    ConstituentSolvents)
global fill

Table = readtable(infile,'Delimiter',',');
data_temp = table2struct(Table);
numrows = length(data_temp);

for i = 1:numrows
    Weight_Weight = str2num(data_temp(i).Weight_Weight);
    data_temp(i).Weight_Weight = Weight_Weight;
    DG_Trans = str2num(data_temp(i).Delta_G_Transfer);
    data_temp(i).DG_Trans = DG_Trans; 
end
data_temp = rmfield(data_temp,'Delta_G_Transfer');
k = 1;
kk = 1;
ions = struct('Ion', {});
add_weight = 0:.05:.6;
for i = 1:length(data_temp)
    [~,index] = ismember(data_temp(i).Ion,{Ion_info.Name});
    Weight = data_temp(i).Weight_Weight./100;
    DG_Trans = data_temp(i).DG_Trans; 
    fit = polyfit(Weight,DG_Trans,2);
    interpolate(i) = struct('Ion',data_temp(i).Ion,'Fit',fit);
    
    index2 = find(not(ismember(add_weight,Weight)));
    
    for j = 1:length(data_temp(i).Weight_Weight)
        data(k) = struct('Ion', data_temp(i).Ion, ...
            'Weight', data_temp(i).Weight_Weight(j)/100, 'Charge', ...
            Ion_info(index).q, 'Radius', Ion_info(index).r, ...
            'Eps_s', Eps_Fun(data_temp(i).Weight_Weight(j)/100), ...
            'DG_Exp', Ref_DG(index)+data_temp(i).DG_Trans(j),'DG_Trans_Exp', ...
        data_temp(i).DG_Trans(j),'Eps_s_Neat_water', Eps_Fun(data_temp(i).Weight_Weight(1)/100));
        if not(ismember(data_temp(i).Ion,{ions.Ion}));
            ions(kk) = struct('Ion',data_temp(i).Ion);
            kk = kk + 1;
        end
        k = k + 1;
    
        % Creating a seperate structured array that houses the interpolated
        % data
        
    end
    
    if fill  
        for w = 1:length(index2)
       
           data(k) = struct('Ion', data_temp(i).Ion, ...
                'Weight', add_weight(index2(w)), 'Charge', ...
                Ion_info(index).q, 'Radius', Ion_info(index).r, ...
                'Eps_s', Eps_Fun(add_weight(index2(w))), ...
                'DG_Exp', Ref_DG(index)+polyval(interpolate(i).Fit,add_weight(index2(w))),'DG_Trans_Exp', ...
            polyval(interpolate(i).Fit,add_weight(index2(w))),'Eps_s_Neat_water', Eps_Fun(data_temp(i).Weight_Weight(1)));
            k = k + 1;
        end
    end
end

fileID = fopen(outfile,'w');

fprintf(fileID, '%40s \n', [ConstituentSolvents{1},' - ',ConstituentSolvents{2},' mixtures']);
fprintf(fileID, '%4s %12s %12s %12s %11s %11s \n', 'Ion,', 'Weight,', ...
    'Charge,', 'Radius,', 'Eps_s,', 'DG_Exp');
for i = 1:length(data)
  fprintf(fileID,'%4s %10s %11s %14s %12s %10s \n', [data(i).Ion, ','], ...
      [num2str(data(i).Weight), ','], [num2str(data(i).Charge), ','], ...
      [num2str(data(i).Radius), ','], [num2str(data(i).Eps_s), ','], ...
      [num2str(data(i).DG_Exp)]);
end

fclose('all');