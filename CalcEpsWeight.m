function [EpsFun] = CalcEpsWeight(Weight_Weight,EpsValues,degree,start,finish,stepsize,changingSolvent)
global plot_toggle
eps_fit = polyfit(Weight_Weight,EpsValues,degree);
Arb_X = linspace(start,finish,stepsize);
eps_evaluated = polyval(eps_fit,Arb_X);
if plot_toggle == 2
    plot(Weight_Weight,EpsValues,'ro',Arb_X,eps_evaluated,'b--')
    xlabel(['Weight Weight % ',changingSolvent]);
    ylabel('\epsilon (Concentration)');
    title('\epsilon As a Function of Weight Weight %')
    figure
end
EpsFun = @(X) eps_fit(1).*(X.^2)+eps_fit(2).*X+eps_fit(end);


