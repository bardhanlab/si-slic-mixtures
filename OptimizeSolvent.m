function [Solvent_Total_Info, SolventTable, Solvent_Ions, Solvent_Ref_Born, ...
    Calc_Solvent, Actual_Solvent, Difference_Solvent, Solvent_Params, ...
    Solvent_Ion_Struct, Solvent_Calc_Born, SolventNLBCTanh, Solvent_Ref_Water, Solvent_Param_Fun, Final_Diff] = OptimizeSolvent(InitialFile, ...
    EpsFun_Solvent, Ion_Ref, Wat_EXP, OutputFile, ConstituentSolvents) 
    
% Ion info 
[Solvent_Total_Info, SolventTable, Solvent_Ions] = read_DG_dat(InitialFile,EpsFun_Solvent,Ion_Ref,Wat_EXP,OutputFile,ConstituentSolvents);

% Optimization
[Solvent_Ref_Born] = Mixture_Optimization_Init(ConstituentSolvents(1),'Solvents.txt',Solvent_Total_Info);
[Calc_Solvent, Actual_Solvent, Difference_Solvent, Solvent_Params, Solvent_Ion_Struct, Solvent_Calc_Born, SolventNLBCTanh, Solvent_Ref_Water, Final_Diff] =  Mixture_Optimization(Solvent_Total_Info, ConstituentSolvents, Solvent_Ref_Born);
% writeResults2('Output/MeOH_Results.tex',MeOH_total_info,Calculated_Results_MeOH,ConstituentSolventsMeOH,predict,'range',[weight_low,weight_high,weight_step]);
Solvent_Param_Fun = Coeffs_to_Fun(Solvent_Params);
