function [Comp, Concentration,DG_Trans_Born,Param_Radius, Water_Ref, DG_NLBC_TANH] = predictDGTransVarriedConcentration(total_ions,funEps_s,funParams,solventType,Wat_Ref,ions,Born_Ref,ion_ref,path,file)
predict = 1;
global eps_in conv_factor phi_toggle temp export plot_toggle
tol=1e-15; 
Water_Ref = [];
Born_Reference = [];
varname = inputname(1);
var_Split = strsplit(varname,'_');
Solvent = var_Split{1};
temp = 25;
eps_in = 1;
conv_factor = 332.112 * 4.184;

paramradweights = 0:.1:1;

for i = 1:length(Wat_Ref)
    if not(ismember(Wat_Ref(i),Water_Ref))
       Water_Ref = [Water_Ref Wat_Ref(i)]; 
    end
end

for i = 1:length(Born_Ref)
    if not(ismember(Born_Ref(i),Born_Reference))
       Born_Reference = [Born_Reference Born_Ref(i)]; 
    end
end
eps_hat_neat = (total_ions(1).Eps_s_Neat_water-eps_in)/total_ions(1).Eps_s_Neat_water;

for i = 1:length(ions)
    [q, r, Name] = Get_ion_info(ion_ref,ions(i));
    Ion_names{i} = Name;
    index = find(strcmp(Name,{total_ions.Ion}));
    ConcentrationRange = [total_ions(index).Weight];
    concentrations(i,:) = linspace(min(ConcentrationRange),max(ConcentrationRange),100);
    for j = 1:length(concentrations(i,:))
        total_solvent_info(j) = create_solvent_structure_for_predict(funParams,1,solventType(1),funEps_s);
        Eps=funEps_s(concentrations(i,j));
        eps_hat = (Eps-eps_in)/(Eps);
        eps_hat_p = 0;
        dEps_dT=0;
        deltaT = 1e-4;
        d2Eps_dT2 = 0;
        eps_hat_p_p = (Eps^2*d2Eps_dT2 - 2*dEps_dT^2*Eps +2*(Eps-eps_in)*dEps_dT^2 ...
                - (Eps-eps_in)*d2Eps_dT2*Eps)/Eps^3;
        
          
%         Param_Radius_temp(j,1) = concentrations(i,j);
   
        
       [DG_NLBC_TANH(j,i)]= NLBC_TANH(concentrations(i,j),eps_hat,eps_hat_p,r,tol,q, ...
		  total_solvent_info(j),temp,eps_in,[20 30] ,conv_factor,eps_hat_p_p,predict,concentrations(i,j));

        DG_Born(j,i) = BORN(eps_hat,r,q,conv_factor);

%         Param_Radius_temp(j,i+1) = Param_radius(eps_hat,DG_NLBC_TANH(j,i),q,conv_factor);
        
        Comp(j,i) = -1.*(Water_Ref(i) - DG_NLBC_TANH(j,i));
        DG_Trans_Born(j,i) = -1.*(Born_Reference(i) - DG_Born(j,i));
    end
end

[Ions_there] = ismember({ion_ref.Name},Ion_names);
for i2 = 1:length(Ions_there)
    if not(Ions_there(i2))
        q = ion_ref(i2).q;
        r = ion_ref(i2).r;
        concentrations_pred(i2,:) = linspace(0,1,100);
        for j = 1:length(concentrations_pred(i2,:))
            total_solvent_info(j) = create_solvent_structure_for_predict(funParams,1,solventType(1),funEps_s);
            Eps=funEps_s(concentrations_pred(i2,j));
            eps_hat = (Eps-eps_in)/(Eps);
            Eps_Neat=funEps_s(0);
            eps_hat_Neat = (Eps_Neat-eps_in)/(Eps_Neat);
            eps_hat_p = 0;
            dEps_dT=0;
            deltaT = 1e-4;
            d2Eps_dT2 = 0;
            eps_hat_p_p = (Eps^2*d2Eps_dT2 - 2*dEps_dT^2*Eps +2*(Eps-eps_in)*dEps_dT^2 ...
                    - (Eps-eps_in)*d2Eps_dT2*Eps)/Eps^3;
            Param_Radius_temp_pred(j,1) = concentrations_pred(i2,j);

            [DG_Trans_SLIC_PRED(j,i2)]= NLBC_TANH(concentrations_pred(i2,j),eps_hat,eps_hat_p,r,tol,q, ...
              total_solvent_info(j),temp,eps_in,[20 30] ,conv_factor,eps_hat_p_p,predict,concentrations_pred(i2,j)) - ...
              NLBC_TANH(0,eps_hat_Neat,eps_hat_p,r,tol,q, ...
              total_solvent_info(j),temp,eps_in,[20 30] ,conv_factor,eps_hat_p_p,predict,0);

            DG_Trans_Born_pred(j,i2) = BORN(eps_hat,r,q,conv_factor) - BORN(eps_hat_Neat,r,q,conv_factor);

        end
        if plot_toggle == 1
            plot(transpose(concentrations_pred(i2,:)),DG_Trans_SLIC_PRED(:,i2),'k--','Linewidth',3');
            hold on
            plot(transpose(concentrations_pred(i2,:)),DG_Trans_Born_pred(:,i2),'r--','Linewidth',3');
            xlabel(['Weight Weight Percentage of ', Solvent],'FontSize',20);
            ylabel('\Delta G_{tr}^{\circ}','FontSize',20);
            title(['\Delta G_{tr_{exp}}^{\circ} of several different models ',Solvent,' for ',ion_ref(i2).Name],'FontSize',20); 
            set(gca,'Fontsize',20);
            if export == 1
                filename = sprintf([path,'/%s.pdf'],[Solvent,'_',ion_ref(i2).Name]);
                export_fig(filename,'-painters','-transparent');
            end
            figure 
        end
    end
end



for i = 1:length(ions)
    [q, r, Name] = Get_ion_info(ion_ref,ions(i));
    for j = 1:length(paramradweights)
        total_solvent_info(j) = create_solvent_structure_for_predict(funParams,1,solventType(1),funEps_s);
        Eps=funEps_s(paramradweights(j));
        eps_hat = (Eps-eps_in)/(Eps);
        eps_hat_p = 0;
        dEps_dT=0;
        deltaT = 1e-4;
        d2Eps_dT2 = 0;
        eps_hat_p_p = (Eps^2*d2Eps_dT2 - 2*dEps_dT^2*Eps +2*(Eps-eps_in)*dEps_dT^2 ...
                - (Eps-eps_in)*d2Eps_dT2*Eps)/Eps^3;
        
          
        Param_Radius_temp(j,1) = paramradweights(j);
   
        
       [DG_NLBC_TANH_rad(j,i)]= NLBC_TANH(paramradweights(j),eps_hat,eps_hat_p,r,tol,q, ...
		  total_solvent_info(j),temp,eps_in,[20 30] ,conv_factor,eps_hat_p_p,predict,paramradweights(j));

        Param_Radius_temp(j,i+1) = Param_radius(eps_hat,DG_NLBC_TANH_rad(j,i),q,conv_factor);
        
    end
end


if export ==  1
    matrix2latex(Param_Radius_temp(:,2:end),[file,Solvent,'_Born_Radii.tex'],'rowLabels',Param_Radius_temp(:,1),'alignment','c','columnLabels',Ion_names,'Solvents',{Solvent,'W'},'Predict','Born Radii');
end

Param_Radius_Table = array2table(Param_Radius_temp,'VariableNames',['Concentration', Ion_names]);
Param_Radius = table2struct(Param_Radius_Table);
Concentration=concentrations;