function Table = writePredict(file,Ions)
Table = struct2table(Ions);
cellsstruct = table2cell(Table);
Results_SLIC = [];
Results_BORN = [];
counter = 1;
for i = 1:length(cellsstruct)
    Results_SLIC = [Results_SLIC cell2mat(cellsstruct(i,3))];
    Results_BORN = [Results_BORN cell2mat(cellsstruct(i,4))];
    for j = 1:length(Ions(i).Ions_Missing)
        colNames(counter) = struct('Name',strjoin([Ions(i).Solvent,'-',Ions(i).Ions_Missing(j)]));
        counter = counter + 1;
    end
end


matrix2latex(round(Results_SLIC,2),file,'rowLabels',Ions(1).Concentrations,'size','tiny','alignment','c','columnLabels',colNames,'Predict','Non Param Ions');
