function [EXP_G_Initial_Solvent] = GetExpDG(fullSolventFile,initializingSolvent)

try
    mix_solvents = read_solvent_data(fullSolventFile,initializingSolvent);
    EXP_G_Initial_Solvent = mix_solvents.EXP_G;
catch
    EXP_G_Initial_Solvent = ones(1,length(mix_solvents(end).EXP_G));
    display(['Initializing solvent not found in ',fullSolventFile,'Reference will be set to ones']);

end

EXP_G_Initial_Solvent = EXP_G_Initial_Solvent(EXP_G_Initial_Solvent~=0);
