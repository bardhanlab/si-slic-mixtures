function [Prediction] = PredictIonsNotPresent(funEps_s_list,funParams_list,exp_list,concentrationRange,ConstituentSolvents,ion_ref)
global eps_in conv_factor export order_toggle fill
tol = 1e-15;
for i = 1:length(funEps_s_list)
    counter = 1;
    Eps_fun = funEps_s_list{i};
    Params = funParams_list{i};
    Exp = exp_list{i};
    ionsmissing = {};
    for j = 1:length(ion_ref)
      ionThere = ismember(ion_ref(j).Name,{Exp.Ion});
      if not(ionThere)
          q = ion_ref(j).q;
          r = ion_ref(j).r;
          ionsmissing{j} = ion_ref(j).Name;
          for k = 1:length(concentrationRange)
              Eps = Eps_fun(concentrationRange(k));
              Eps_neat = Eps_fun(0); 
              eps_hat = (Eps-eps_in)/(Eps);
              eps_hat_neat = (Eps_neat-eps_in)/Eps_neat;
              eps_hat_p = 0;
              dEps_dT=0;
              deltaT = 1e-4;
              d2Eps_dT2 = 0;
              eps_hat_p_p = (Eps^2*d2Eps_dT2 - 2*dEps_dT^2*Eps +2*(Eps-eps_in)*dEps_dT^2 ...
                - (Eps-eps_in)*d2Eps_dT2*Eps)/Eps^3;

              SLIC_DG_Trans(k,counter,i)= NLBC_TANH(concentrationRange(k),eps_hat,eps_hat_p,r,tol,q, ...
                Params,25,eps_in,[20 30] ,conv_factor,eps_hat_p_p,1,concentrationRange(k)) - ...
                NLBC_TANH(0,eps_hat_neat,eps_hat_p,r,tol,q, ...
                Params,25,eps_in,[20 30] ,conv_factor,eps_hat_p_p,1,0);

              Born_DG_Trans(k,counter,i) = BORN(eps_hat,r,q,conv_factor)-BORN(eps_hat_neat,r,q,conv_factor);
          end
      counter = counter + 1;    
      end
      if j == length(ion_ref)
          ionNotThere(i) = struct('Solvent',ConstituentSolvents{i}, ...
              'Ion_Not_Present',{ionsmissing});
      end  
    end
end

for i = 1:length(SLIC_DG_Trans(1,1,:))
   SLIC = SLIC_DG_Trans(:,:,i);
   SLIC_index = find(not(all(SLIC==0)));
   Born = Born_DG_Trans(:,:,i);
   Born_index = find(not(all(Born==0)));
   Prediction(i) = struct('Solvent',ionNotThere(i).Solvent,'Ions_Missing',...
       {ionNotThere(i).Ion_Not_Present(~cellfun('isempty',ionNotThere(i).Ion_Not_Present))},...
       'SLIC_Predicted_DG_Trans',{SLIC(:,SLIC_index)},'Born_Predicted_DG_Trans',{Born(:,Born_index)},...
       'Concentrations',{concentrationRange'});
end
if export == 1
    if order_toggle == 1 && fill == 0
        path = 'Output/Global_Fit_Concentrations_0-1_Linear';
    elseif order_toggle == 1 && fill == 1
        path = 'Output/Global_Fit_Concentrations_0-1_Linear_filled';
    elseif order_toggle == 2 && fill == 0
        path = 'Output/Global_Fit_Concentrations_0-1';
    elseif order_toggle == 2 && fill == 1
        path = 'Output/Global_Fit_Concentrations0-1_Quad_fill';
    end
    writePredict([path,'/Predict_Ions.tex'],Prediction);
end
