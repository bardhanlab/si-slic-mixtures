%% Dioxan as a function of Temp from Akerlof and Short Table 1
function Fun_Concentration = ComputingDioxEps(~)

degree = 2;
Temp0 = linspace(0,80,9);
Eps0 = [88.31 84.25 80.37 76.73 73.12 69.85 66.62 63.50 60.58];
[Coeffs0, Fun0] = SolventEps(Temp0,Eps0,degree);
Diox_25C_0wt = Fun0(25); 

Temp10 = linspace(0,80,9);
Eps10 = [78.86 75.06 71.43 67.98 64.70 61.57 68.60 55.77 53.07];
[Coeffs10, Fun10] = SolventEps(Temp10,Eps10,degree);
Diox_25C_10wt = Fun10(25);

Temp20 = linspace(0,80,9);
Eps20 = [69.16 65.68 62.38 59.24 56.26 53.43 50.75 48.20 45.77];
[Coeffs20, Fun20] = SolventEps(Temp20,Eps20,degree);
Diox_25C_20wt = Fun20(25);

Temp30 = linspace(0,80,9);
Eps30 = [59.34 56.24 53.30 50.52 47.88 45.38 43.01 40.76 38.63];
[Coeffs30, Fun30] = SolventEps(Temp30,Eps30,degree);
Diox_25C_30wt = Fun30(25);

Temp40 = linspace(0,80,9);
Eps40 = [49.37 46.71 44.19 41.80 39.54 37.41 35.39 33.48 31.67];
[Coeffs40, Fun40] = SolventEps(Temp40,Eps40,degree);
Diox_25C_40wt = Fun40(25);

Temp50 = linspace(0,80,9);
Eps50 = [39.50 37.31 35.25 33.30 31.46 29.72 28.08 26.52 25.05];
[Coeffs50, Fun50] = SolventEps(Temp50,Eps50,degree);
Diox_25C_50wt = Fun50(25);

Temp60 = linspace(0,80,9);
Eps60 = [29.84 28.17 26.60 25.12 23.72 22.40 21.15 19.97 18.86];
[Coeffs60, Fun60] = SolventEps(Temp60,Eps60,degree);
Diox_25C_60wt = Fun60(25);

Temp70 = linspace(0,80,9);
Eps70 = [20.37 19.25 18.20 17.20 16.26 15.37 14.52 13.73 12.97];
[Coeffs70, Fun70] = SolventEps(Temp70,Eps70,degree);
Diox_25C_70wt = Fun70(25);

Temp80 = linspace(0,80,9);
Eps80 = [12.19 11.58 10.99 10.44 9.91 9.41 8.93 8.48 8.05];
[Coeffs80, Fun80] = SolventEps(Temp80,Eps80,degree);
Diox_25C_80wt = Fun80(25);

Temp90 = linspace(0,80,9);
Eps90 = [6.16 5.93 5.71 5.50 5.30 5.10 4.91 4.73 4.56];
[Coeffs90, Fun90] = SolventEps(Temp90,Eps90,degree);
Diox_25C_90wt = Fun90(25);

Temp95 = linspace(0,80,9);
Eps95 = [3.91 3.82 3.74 3.65 3.57 3.49 3.41 3.33 3.25];
[Coeffs95, Fun95] = SolventEps(Temp95,Eps95,degree);
Diox_25C_95wt = Fun95(25);

Temp98 = linspace(0,80,9);
Eps98 = [2.73 2.70 2.68 2.65 2.62 2.60 2.57 2.55 2.52];
[Coeffs98, Fun98] = SolventEps(Temp98,Eps98,degree);
Diox_25C_98wt = Fun98(25);

Temp100 = linspace(0,80,9);
Eps100 = [2.109 2.104 2.102 2.1 2.098 2.096 2.094 2.092 2.090];
[Coeffs100, Fun100] = SolventEps(Temp100,Eps100,degree);
Diox_25C_100wt = Fun100(25);
%% Calculating Eps Glycerol at 25 C as a func of Concentration

Concentrations = [linspace(0,90,10), 95 98 100]./100;
Eps_vals = [Diox_25C_0wt, Diox_25C_10wt, Diox_25C_20wt, Diox_25C_30wt, ...
    Diox_25C_40wt, Diox_25C_50wt, Diox_25C_60wt, Diox_25C_70wt, Diox_25C_80wt, ...
    Diox_25C_90wt, Diox_25C_95wt, Diox_25C_98wt, Diox_25C_100wt];

[Coeffs_Concentration, Fun_Concentration] = SolventEps(Concentrations,Eps_vals,degree);
foo = 0;