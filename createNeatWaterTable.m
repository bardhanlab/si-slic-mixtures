function [T,DG_NLBC_TANH] = createNeatWaterTable(ParamCoeffs, EpsFun, Ion_Ref, Solvents, file, varargin)
global eps_in conv_factor export  
predict = 1;
tol=1e-15; 

for i = 1:length(ParamCoeffs)
    Coeffs = ParamCoeffs{i};
    EpsFun_sol = EpsFun{i};
    Eps = EpsFun_sol(0);
    eps_hat = (Eps-eps_in)/(Eps);
    eps_hat_p = 0;
    dEps_dT=0;
    deltaT = 1e-4;
    d2Eps_dT2 = 0;
    eps_hat_p_p = (Eps^2*d2Eps_dT2 - 2*dEps_dT^2*Eps +2*(Eps-eps_in)*dEps_dT^2 ...
        - (Eps-eps_in)*d2Eps_dT2*Eps)/Eps^3;
    
    for j = 1:length(Ion_Ref)
        q = Ion_Ref(j).q;
        r = Ion_Ref(j).r;
        [DG_NLBC_TANH(i,j)]= NLBC_TANH(0,eps_hat,eps_hat_p,r,tol,q, ...
              Coeffs,25,eps_in,[20 30] ,conv_factor,eps_hat_p_p,predict,0);
    end 
end

if export == 1
    matrix2latex(DG_NLBC_TANH,[file,'Delta_G_Solvation_Neat_Water.tex'],'rowLabels',Solvents','alignment','c','columnLabels',{Ion_Ref.Name},'Predict','Neat Water');
end

A = cell2table(Solvents','VariableNames',{'Solvents'});
B = array2table(DG_NLBC_TANH,'VariableNames',{Ion_Ref.Name});
T = [A, B];
