function [Weight_Weight] = VolToWeight(Vol_fracs,Solvent1ConvFactor,Solvent2ConvFactor)

Mol_frac_Solvent1 = Vol_fracs;
Mol_frac_Solvent2 = 100-Mol_frac_Solvent1;

Solvent1_mass = Mol_frac_Solvent1*Solvent1ConvFactor;
Solvent2_mass = Mol_frac_Solvent2*Solvent2ConvFactor;

Total_mass = Solvent1_mass + Solvent2_mass;

Weight_Weight = Solvent1_mass./Total_mass;