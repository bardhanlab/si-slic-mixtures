function [deltaG,Ion_struct] = Calc_DeltaG_exp(temp,solvent,index,init,ions)
eps_in=1;


global conv_factor Ion_Data EXP_G_dat EXP_S_dat EXP_HEATCAP_dat    
delt = temp-25;

[Ion_Data,EXP_G_dat,EXP_S_dat,EXP_HEATCAP_dat] = Get_Ion_Data_Amir(solvent,init,ions);
Ion_struct = Ion_Data;
    for j=1:length(Ion_Data) 
        deltaG(index,j)=(EXP_G_dat(j))/conv_factor; %-delt*EXP_S_dat(j)/1000-EXP_HEATCAP_dat(j)/(298.15*2*1000)*(delt^2))/conv_factor;
    end

deltaG = deltaG';
spencer = 1;