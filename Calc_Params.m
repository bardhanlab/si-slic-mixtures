function [alpha, beta, gamma, mu, phi_stat] = Calc_Params(weight, alpha_vect, ...
    beta_vect, gamma_vect, mu_vect, phi_stat_vect)
global order_toggle

if order_toggle == 2
alpha = alpha_vect(1).*weight.^2 + alpha_vect(2).*weight + alpha_vect(3);
beta = beta_vect(1).*weight.^2 + beta_vect(2).*weight + beta_vect(3);
gamma = gamma_vect(1).*weight.^2 + gamma_vect(2).*weight + gamma_vect(3);
mu = mu_vect(1).*weight.^2 + mu_vect(2).*weight + mu_vect(3);
phi_stat = phi_stat_vect(1).*weight.^2 + phi_stat_vect(2).*weight + phi_stat_vect(3);
elseif order_toggle == 1
alpha = alpha_vect(1).*weight + alpha_vect(2);
beta = beta_vect(1).*weight + beta_vect(2);
gamma = gamma_vect(1).*weight + gamma_vect(2);
mu = mu_vect(1).*weight + mu_vect(2);
phi_stat = phi_stat_vect(1).*weight + phi_stat_vect(2);
   
end

% alpha = alpha_vect(1).*weight.^2 + alpha_vect(2).*weight + 0.0988559374840066;
% beta = beta_vect(1).*weight.^2 + beta_vect(2).*weight + 41.2467242078621;
% gamma = gamma_vect(1).*weight.^2 + gamma_vect(2).*weight + 19.8172137324093;
% mu = mu_vect(1).*weight.^2 + mu_vect(2).*weight + 0.405895271807237;
% phi_stat = phi_stat_vect(1).*weight.^2 + phi_stat_vect(2).*weight - 0.00792594052764437;

% alpha = alpha_vect(1).*weight + 0.0988559374840066;
% beta = beta_vect(1).*weight + 41.2467242078621;
% gamma = gamma_vect(1).*weight + 19.8172137324093;
% mu = mu_vect(1).*weight + 0.405895271807237;
% phi_stat = phi_stat_vect(1).*weight - 0.00792594052764437;

% alpha = alpha_vect(1).*weight + 0.09940595567;
% beta = beta_vect(1).*weight + 41.248099259;
% gamma = gamma_vect(1).*weight + 19.81692877502;
% mu = mu_vect(1).*weight + 0.40579551105;
% phi_stat = phi_stat_vect(1).*weight - 0.00802203755;