function [DG_BORN] = Mixture_Optimization_Init(solventType,solventData,ions)
% addpath('../pnas2016/Phi_toggled')
% addpath('../pnas2016/Phi_toggled/export_fig/')
init = 1;
predict = 0;
% set the following to 0 (zero) if you don't want to save each
% individual figure automatically, according to the names set
% inside the code.
 

global solvent_type solvent phi_toggle eps_in conv_factor FID errorgraph FID_PREDICT FID2 printOn hfuncgraph
FID=fopen('param_data.txt','w');
FID_PREDICT=fopen('predict.txt','w');
FID2=fopen('parinfo.txt','w');
errorgraph=0;
heatcapgraph=0;
deltaggraph=0;
deltasgraph=0;
hfuncgraph=0;
printOn = 0;

solvent_type = solventType; % choose the solvents you would like, otherwise 'Water','MeOH','F','AN','DMF'
solvent = read_solvent_data(solventData,solvent_type);

eps_in=1;
conv_factor = 332.112 * 4.184;      % from Angstroms/relative
                                        % permittivities/electron charges                                        % into kJ/mol;

% clearvars   dphidn_HSBC_MSA sigma_over_dphidn_HSBC_MSA ...
%             dphidn_NLBC_TANH sigma_nom_over_dphidn_NLBC_TANH...
%             dphidn_BORN sigma_over_dphidn_BORN ...
%             DG_HSBC_MSA DS_HSBC_MSA...
%             DG_Fawcett_Asym DS_Fawcett_Asym dphidn_Fawcett_Asym sigma_over_dphidn_Fawcett_Asym...
%             DG_Fawcett_Sym DS_Fawcett_Sym dphidn_Fawcett_Sym sigma_over_dphidn_Fawcett_Sym...
%             DG_NLBC_TANH DS_NLBC_TANH...        
%             DG_BORN DS_BORN CP_BORN...
%             CP_NLBC_TANH 


phi_toggle=1;

for j=1:length(ions);
    eps_hat = (ions(j).Eps_s_Neat_water-eps_in)/(ions(j).Eps_s_Neat_water);
    q=ions(j).Charge;
    r=ions(j).Radius;
    [DG_BORN(j)]= ...                               %Function for symmetric BORN
        BORN(eps_hat,r,q,conv_factor);
end


% [SLIC_DG,~,~,DG_BORN] = Calcsolventmodels_MSA(1,init,ions,predict,'foo');